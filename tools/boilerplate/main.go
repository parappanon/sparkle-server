package main

import (
	"flag"
	"log"
	"os"
	"strings"
	"text/template"
	"unicode"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

/*
 * toLowerCamelCase converts a string to lower camel case.
 * LICENSE: MIT
 * Author: Poe Sage (https://poe.com/sage)
 */
func toLowerCamelCase(word string) string {
	if len(word) == 0 {
		return ""
	}

	// Replace underscores and hyphens with spaces
	word = strings.ReplaceAll(word, "_", " ")
	word = strings.ReplaceAll(word, "-", " ")

	// Split the words and convert to LowerCamelCase
	words := strings.Fields(word)
	for i, w := range words {
		runes := []rune(w)
		if i == 0 {
			runes[0] = unicode.ToLower(runes[0])
		} else {
			runes[0] = unicode.ToUpper(runes[0])
		}
		words[i] = string(runes)
	}

	// Join the words and return the result
	return strings.Join(words, "")
}

func main() {
	flag.Parse()
	snake := flag.Arg(0)
	if snake == "" {
		log.Fatal("please enter snake_case name")
		return
	}
	lowerCamel := toLowerCamelCase(snake)
	upperCamel := cases.Title(language.English, cases.NoLower).String(lowerCamel)

	files := []struct {
		fn   string
		tmpl string
		fp   string
		data map[string]string
	}{
		{
			fn:   snake + "_model.go",
			tmpl: "model.tmpl",
			fp:   "domain/model/",
			data: map[string]string{"UpperCamelCase": upperCamel},
		},
		{
			fn:   snake + "_persistence.go",
			tmpl: "persistence.tmpl",
			fp:   "infrastructure/persistence/",
			data: map[string]string{
				"LowerCamelCase": lowerCamel,
				"UpperCamelCase": upperCamel,
			},
		},
		{
			fn:   snake + "_repository.go",
			tmpl: "repository.tmpl",
			fp:   "domain/repository/",
			data: map[string]string{"UpperCamelCase": upperCamel},
		},
		{
			fn:   snake + "_usecase.go",
			tmpl: "usecase.tmpl",
			fp:   "application/usecase/",
			data: map[string]string{
				"LowerCamelCase": lowerCamel,
				"UpperCamelCase": upperCamel,
			},
		},
	}

	for _, f := range files {
		t, err := template.ParseFiles("boilerplate/templates/" + f.tmpl)
		if err != nil {
			log.Println(err)
		}

		fp, err := os.Create(f.fp + f.fn)
		if err != nil {
			log.Println("error creating"+f.fp+"file", err)
		}
		defer fp.Close()

		if err = t.Execute(fp, f.data); err != nil {
			log.Println(err)
		}
	}
}
