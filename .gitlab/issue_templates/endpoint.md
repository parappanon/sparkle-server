<!--
Please Copy below and use it to issue name template:
[ENDPOINT] Implement /player/<PATH NAME>
-->

## Abstract

<!--
  Describe what does the endpoint do
  (Ex. Get player data / Get player's support character parties )
-->

- Implement a endpoint to [DO SOMETHING].

<!--
 Describe about why it needed
 (Ex. offline data creation / new-register / login / for-fun / for-web-ui )
-->

- This one is for [DO SOMETHING].

## Tasks

<!--
 These are basic tasks to implement the new endpoint.
 Please add if extra task is needed.
-->

- [ ] Make a database
  <!-- the gen means domain/model/gen/model -->
  - [ ] Move a model from gen
  <!-- the model means domain/model -->
  - [ ] Create models
    - [ ] Enum
    - [ ] Base models
    - [ ] Sub models
    <!-- the migration means database/migrate -->
  - [ ] Add models to migration
  <!-- the seeding means database/seed -->
  - [ ] Add seeding
    - [ ] Make convert script
    - [ ] Create seed file
    - [ ] Upload to the server
- [ ] Implement logics
  <!-- the repository means domain/repository -->
  - [ ] Repository layer (interface)
  <!-- the persistence means infrastructure/persistence -->
  - [ ] Persistence layer (database access)
  <!-- the usecase means application/usecase -->
  - [ ] Usecase layer (core logic system)
    - [ ] Test
    <!-- the presentation means interfaces/_service.go -->
  - [ ] Presentation layer (call a usecase)
    - [ ] Transfer to response (db and response)
    - [ ] Test
- [ ] Add the main.go

## References

<!--
  These are template links to do thing.
 Please add if extra reference are existed.
 -->

- https://kirafan.gitlab.io/sparkle/document-web/Models/[ENDPOINT NAME]Response.html
- https://gitlab.com/kirafan/database/-/tree/master/database
