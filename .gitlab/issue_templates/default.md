### Overview / 概要

<!-- Describe like twitter tweet (max 255 characters) / ツイッターのツイートぐらいの文量での説明 -->

### Details / 詳細

<!-- More detailed infos (why-needed / how-to / tasks) -->

### Related issues / 関連 Issue

<!-- Add a issue id if there is related issue, otherwise you can remove section -->

### References / 参考文献

<!-- Add if there is a stack-overflow link, kirafan doc or something exists. discord link is also ok. -->
