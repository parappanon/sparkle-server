## Sparkle-server

[![Golang](https://img.shields.io/badge/Go-00ADD8?style=flat&logo=go&logoColor=white)](https://go.dev)
[![VSCode](https://img.shields.io/badge/-Visual%20Studio%20Code-007ACC.svg?logo=visual-studio-code&style=flat)](https://code.visualstudio.com/)
[![AGPL](https://img.shields.io/github/license/ad-aures/castopod?color=blue)](https://www.gnu.org/licenses/agpl-3.0.en.html)
[![total coding time](https://wakatime.com/badge/user/4893740b-9141-4a49-aec6-eb82b6367166/project/a7859e86-9b9c-4c8b-afd8-cf6e3b2886e1.svg)](https://wakatime.com)

![image](./docs/icon.png)

**Let’s draw the continuation of the story with you**

Star-Api emulation server

## Progress

- [ ] Account
  - [x] Create
  - [x] Login
  - [x] Login bonus
  - [x] Transfer
  - [x] Stamina
  - [ ] Profile
  - [ ] Achievement
  - [ ] Support party
  - [x] Battle party
  - [ ] Town party
- [ ] Weapon
  - [ ] Make
  - [ ] Upgrade
  - [ ] Evolution
  - [ ] Receive
  - [ ] Sale
- [ ] Character
  - [x] Upgrade
  - [ ] Evolution
  - [ ] Limitbreak
  - [ ] ResetView
  - [ ] Ability board
- [ ] Room
  - [x] Buy/Sale/Set room objects
  - [x] Schedule (static)
  - [ ] Content room
- [x] Town
- [ ] Friend
- [ ] Training
- [ ] Mission
  - [ ] Refresh
  - [ ] Progress
  - [ ] Complete
- [ ] Present
  - [ ] Insert
  - [x] Receive (initial)
  - [ ] Receive (basic)
  - [ ] Received list
- [ ] Trade
  - [ ] Item sale
  - [ ] Monthly trade
  - [ ] Gem trade
  - [ ] Event trade
- [ ] Gacha
  - [x] Tutorial gacha
  - [x] Selectable gacha
  - [x] Normal gacha
    - [ ] Gacha gifts
    - [ ] Draw points
- [ ] Quest
  - [x] Core
    - [x] Start
    - [x] Save
    - [x] Complete
  - [x] Option
    - [x] Resume
    - [x] Retry
    - [x] Re-challenge
    - [x] Give-up
  - [ ] List
    - [x] Part1 quests
      - [x] Chapters
    - [x] Part2 quests
      - [x] Chapters
    - [ ] Author quests
    - [ ] Daily quests
    - [ ] Weekend quests (challenge quests)
    - [ ] Memorial quests
    - [ ] Crea quests
    - [ ] Event quests
      - [ ] Normal event
      - [ ] Box event
      - [ ] Point event
      - [ ] Map event
- [x] System
  - [x] App version
  - [x] Information
  - [x] Event banner

## Architecture

- DDD (Domain Driven Development)
- API framework: Golang net/http + mux.Router
- ORM: gorm
- Database: MariaDB / MySQL / SQLite

## Requirements (Development)

- VSCode
- Golang 1.20.2
- Docker (Optional)
- Air (Optional)

## Requirements (Production)

- MariaDB (or MySQL)
- Docker (Optional)

### Running the server (Development)

To run the server, follow these simple steps:

```
cp .env_example .env
(Write your .env)
go run cmd/main/main.go
```

If you need hot-reload, follow these steps:

```
Install Docker-Desktop
Open repository folder in VSCode
Run "Remote-Containers: Reopen in Container"
Run "air"
```

### Running the server (Production)

See [docker-compose.yml](./docker-compose.yml), or download binary from release page.

## Contributing

To contribute to the server, follow these simple steps:

- Fork a code and make your repository.
  - This project doesn't allow direct push currently.
- Read the docs at some folders, and read issues about what we needs next.
  - This project uses DDD architecture, so you need to understand the how it works.
- Find the issue you want to do, and assign yourself.
  - Please contact to Dosugamea first if you have time.
  - It will helps to preventing file conflicts.
- Push your modified code to your private repository.
- Make a pull request to the original repository.
- Wait for the review and merge.
- After a approve, the code will be merged to the original repository.

## Contributors

- Server development
  - [Dosugamea](https://gitlab.com/Dosugamea)
  - [lihe](https://gitlab.com/lihe07)
- Data mining / Decryption
  - [Y52en](https://gitlab.com/Y52en)
  - [Youko](https://gitlab.com/Y2theK)
  - [CircleLin](https://gitlab.com/circlelin)
  - [Ayaya](https://gitlab.com/kirafan_autodec)
- Launcher development
  - 250king
- Tester & suggestion
  - fibachocolate
  - AyayaRize
- Thanks for all contributors. It couldn't done without their a lot of hard works.
  - (Of course, big thanks to original game developers!)

## References

- Analysis
  - https://wiki.kirafan.moe
  - https://gitlab.com/kirafan/database
- Fan wiki
  - https://kirarafantasia.miraheze.org/wiki/Main_Page
  - https://wikiwiki.jp/kirarafan/
- Other wiki
  - https://kirarafantasia.boom-app.wiki
- Big thanks to these websites. It couldn't done without these infos.

## License

AGPL-3.0
