## How to re-generate and overwrite the structs

- 1 Install the openapi generator cli:
  - `npm install -g @openapitools/openapi-generator-cli`
- 2 Copy the `openapi.json` file "outside" of repository folder
- 3 Run the following command:
  - `openapi-generator-cli generate -i openapi.json -g go-server -o ./sparkle-server --additional-properties=packageName=interfaces`
    - Since yaml file is too big to be parsed by the generator (It raises error)
- 4 Move the `sparkle-server/go` folder's files into `sparkle-server/interfaces` folder
- 5 Delete the `sparkle-server/openapi.yaml`
  - Since yaml file is too big to be parsed by the generator (It raises error)
- 6 Move the `openapi.json` file into `sparkle-server` folder and overwrite
- 7 Format generated files with below comamnds
  - `goimports -w .`
  - `go fmt .`
- 8 Rollback some routers changes
  - Since the generator ignores the .openapi-generator-ignore...
- 9 Fix model_event_banner.go
  - IDK why but it is not generated correctly
- 10 Now probably generated structures are up-to-date.
