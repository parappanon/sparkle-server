package interfaces

import (
	"time"

	model_chapter "gitlab.com/kirafan/sparkle/server/internal/domain/model/chapter"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toGetAllQuestChapterResponse(dbQuestChapters []*model_chapter.Chapter, latestTime *time.Time, success *response.BaseResponse) *GetAllQuestChapterResponse {
	outQuestChapters := make([]ChaptersArrayObject, 0)
	calc.Copy(&outQuestChapters, &dbQuestChapters)
	// Format resp->quests->questIds
	for i, chapter := range dbQuestChapters {
		outQuestIds := make([]int64, 0)
		for _, questId := range chapter.QuestIds {
			outQuestIds = append(outQuestIds, int64(questId.QuestId))
		}
		outQuestChapters[i].QuestIds = outQuestIds
		if chapter.NewDispEndAt != nil {
			newDispEndAt := response.ToSparkleTime(*chapter.NewDispEndAt)
			outQuestChapters[i].NewDispEndAt = &newDispEndAt
		}
	}
	newDispEndAt := time.Now()
	if latestTime != nil && !latestTime.IsZero() {
		newDispEndAt = *latestTime
	}
	return &GetAllQuestChapterResponse{
		Chapters:     outQuestChapters,
		NewDispEndAt: response.ToSparkleTime(newDispEndAt),
		// Template responses
		ServerVersion:       success.ServerVersion,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		NewAchievementCount: success.NewAchievementCount,
	}
}
