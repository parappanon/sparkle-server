package interfaces

import (
	"strconv"
	"strings"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toBuyAllPlayerTownFacilityResponse(
	u *model_user.User,
	newFacilityIds []uint,
	success *response.BaseResponse,
) *BuyAllPlayerTownFacilityResponse {
	var managedTownFacilityIds []string
	for _, newFacilityId := range newFacilityIds {
		managedTownFacilityIds = append(managedTownFacilityIds, strconv.Itoa(int(newFacilityId)))
	}
	return &BuyAllPlayerTownFacilityResponse{
		ManagedTownFacilityIds: strings.Join(managedTownFacilityIds, ","),
		Player:                 ToBasePlayerResponse(u),
		NewAchievementCount:    success.NewAchievementCount,
		ResultCode:             success.ResultCode,
		ResultMessage:          success.ResultMessage,
		ServerTime:             success.ServerTime,
		ServerVersion:          success.ServerVersion,
	}
}
