/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type LimitbreakPlayerCharacterRequest struct {
	ItemIds []int64 `json:"itemIds"`

	ManagedCharacterId int64 `json:"managedCharacterId"`
}

// AssertLimitbreakPlayerCharacterRequestRequired checks if the required fields are not zero-ed
func AssertLimitbreakPlayerCharacterRequestRequired(obj LimitbreakPlayerCharacterRequest) error {
	elements := map[string]interface{}{
		"itemIds":            obj.ItemIds,
		"managedCharacterId": obj.ManagedCharacterId,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseLimitbreakPlayerCharacterRequestRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of LimitbreakPlayerCharacterRequest (e.g. [][]LimitbreakPlayerCharacterRequest), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseLimitbreakPlayerCharacterRequestRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aLimitbreakPlayerCharacterRequest, ok := obj.(LimitbreakPlayerCharacterRequest)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertLimitbreakPlayerCharacterRequestRequired(aLimitbreakPlayerCharacterRequest)
	})
}
