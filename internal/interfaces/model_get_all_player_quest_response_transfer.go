package interfaces

import (
	schema_quest "gitlab.com/kirafan/sparkle/server/internal/application/schemas/quest"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// FIXME: There is no way to separate this DTO(database to response model conversion)
// The DTO depends interfaces package's models, but this router also located at interfaces package
// So, I just put this conversion code here.
// If you have any idea, please let me know.

func toGetAllPlayerQuestResponse(res *schema_quest.AllQuestInfoWithClearRanksSchema, success *response.BaseResponse) *GetAllPlayerQuestResponse {
	// Format resp->quests field
	outQuestPart1s := make([]QuestsArrayObject, len(res.AllQuestInfo.Quests))
	for i := range outQuestPart1s {
		var copiedQuest QuestsArrayObjectQuest
		calc.Copy(&copiedQuest, &res.AllQuestInfo.Quests[i])
		calc.Copy(&copiedQuest, &copiedQuest.QuestFirstClearReward)
		copiedQuest.Gem1 = copiedQuest.QuestFirstClearReward.Gem
		copiedQuest.Gold1 = copiedQuest.QuestFirstClearReward.Gold
		// Fill empty array
		if len(copiedQuest.QuestNpcs) == 0 {
			copiedQuest.QuestNpcs = make([]QuestNpcsArrayObject, 0)
		}
		// Fill duplicated value
		copiedQuest.WaveIds = []int64{
			copiedQuest.WaveId1,
			copiedQuest.WaveId2,
			copiedQuest.WaveId3,
			copiedQuest.WaveId4,
			copiedQuest.WaveId5,
		}
		// Fill Mystery values
		copiedQuest.MainFlg = 0
		copiedQuest.Section = -1
		// Finalize QuestsArrayObject
		clearRank := int64(0)
		for i2 := range res.ClearRanks {
			if res.ClearRanks[i2].QuestId == uint(copiedQuest.Id) {
				clearRank = int64(res.ClearRanks[i2].ClearRank)
				break
			}
		}
		outQuestPart1s[i] = QuestsArrayObject{
			ClearRank: clearRank,
			Quest:     copiedQuest,
		}
	}
	// Format resp->questPart2s field
	outQuestPart2s := make([]Questpart2sArrayObject, len(res.AllQuestInfo.QuestPart2s))
	for i := range outQuestPart2s {
		var copiedQuest Questpart2sArrayObjectQuest
		calc.Copy(&copiedQuest, &res.AllQuestInfo.QuestPart2s[i])
		calc.Copy(&copiedQuest, &copiedQuest.QuestFirstClearReward)
		copiedQuest.Gem1 = copiedQuest.QuestFirstClearReward.Gem
		copiedQuest.Gold1 = copiedQuest.QuestFirstClearReward.Gold
		// Fill empty array
		if len(copiedQuest.QuestNpcs) == 0 {
			copiedQuest.QuestNpcs = make([]QuestNpcsArrayObject, 0)
		}
		// Fill duplicated value
		copiedQuest.WaveIds = []int64{
			copiedQuest.WaveId1,
			copiedQuest.WaveId2,
			copiedQuest.WaveId3,
			copiedQuest.WaveId4,
			copiedQuest.WaveId5,
		}
		// Fill Mystery values
		copiedQuest.MainFlg = 0
		copiedQuest.Section = -1
		// Finalize QuestsArrayObject
		clearRank := int64(0)
		for i2 := range res.ClearRanks {
			if res.ClearRanks[i2].QuestId == uint(copiedQuest.Id) {
				clearRank = int64(res.ClearRanks[i2].ClearRank)
				break
			}
		}
		outQuestPart2s[i] = Questpart2sArrayObject{
			ClearRank: clearRank,
			Quest:     copiedQuest,
		}
	}

	return &GetAllPlayerQuestResponse{
		Quests:                    outQuestPart1s,
		QuestPart2s:               outQuestPart2s,
		QuestStaminaReductions:    []QuestStaminaReductionsArrayObject{},
		EventQuestPeriods:         []EventQuestPeriodsArrayObject{},
		CharacterQuests:           []CharacterQuestsArrayObject{},
		EventQuests:               []EventQuestsArrayObject{},
		PlayerOfferQuests:         []PlayerOfferQuestsArrayObject{},
		LastPlayedChapterQuestIds: []int64{int64(res.LastPlayedChapterQuestIdPart1), int64(res.LastPlayedChapterQuestIdPart2)},
		PlayedOpenChapterIds:      []int64{int64(res.PlayedOpenChapterIdPart1), int64(res.PlayedOpenChapterIdPart2)},
		ReadGroups:                make([]interface{}, 0),
		// Template responses
		ServerVersion:       success.ServerVersion,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		NewAchievementCount: success.NewAchievementCount,
	}
}
