package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toGetAllPlayerMissionResponse(missions *[]model_user.UserMission, success *response.BaseResponse) *GetAllPlayerMissionResponse {
	var missionLogs []MissionlogsArrayObject
	for _, userMission := range *missions {
		var rewards []RewardArrayObject
		for _, missionReward := range userMission.Mission.Reward {
			rewards = append(rewards, RewardArrayObject{
				RewardType: int64(missionReward.RewardType),
				RewardNo:   int64(missionReward.RewardNo),
				RewardNum:  int64(missionReward.RewardNum),
			})
		}
		missionLogs = append(missionLogs, MissionlogsArrayObject{
			ManagedMissionId: int64(userMission.ManagedMissionId),
			Category:         int64(userMission.Mission.Category),
			MissionId:        int64(userMission.MissionId),
			MissionFuncType:  int64(userMission.Mission.MissionFuncType),
			MissionSegType:   int64(userMission.Mission.MissionSegType),
			TargetMessage:    userMission.Mission.TargetMessage,
			Reward:           rewards,
			Rate:             int64(userMission.Rate),
			RateMax:          int64(userMission.Mission.RateMax),
			SubCode:          int64(userMission.Mission.SubCode),
			State:            int64(userMission.State),
			PlayerId:         int64(userMission.UserId),
			LimitTime:        response.ToSparkleTime(userMission.LimitTime),
			TransitParam:     int64(userMission.Mission.TransitParam),
			TransitScene:     int64(userMission.Mission.TransitScene),
			UiPriority:       int64(userMission.Mission.UiPriority),
		})
	}
	return &GetAllPlayerMissionResponse{
		MissionLogs:         missionLogs,
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}
}
