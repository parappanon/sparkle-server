package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toSetShownPlayerContentRoomResponse(
	user *model_user.User,
	success *response.BaseResponse,
) *SetShownPlayerContentRoomResponse {
	var outOfferTitleTypes []OfferTitleTypesArrayObject
	calc.Copy(&outOfferTitleTypes, &user.OfferTitleTypes)
	return &SetShownPlayerContentRoomResponse{
		OfferTitleTypes:     outOfferTitleTypes,
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}
}
