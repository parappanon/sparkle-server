/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type SetPlayerRoomRequest struct {
	ArrangeData []ArrangeDataArrayObject `json:"arrangeData"`

	FloorId int64 `json:"floorId"`

	GroupId int64 `json:"groupId"`

	ManagedRoomId int64 `json:"managedRoomId"`
}

// AssertSetPlayerRoomRequestRequired checks if the required fields are not zero-ed
func AssertSetPlayerRoomRequestRequired(obj SetPlayerRoomRequest) error {
	elements := map[string]interface{}{
		"arrangeData":   obj.ArrangeData,
		"floorId":       obj.FloorId,
		"managedRoomId": obj.ManagedRoomId,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	for _, el := range obj.ArrangeData {
		if err := AssertArrangeDataArrayObjectRequired(el); err != nil {
			return err
		}
	}
	return nil
}

// AssertRecurseSetPlayerRoomRequestRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of SetPlayerRoomRequest (e.g. [][]SetPlayerRoomRequest), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseSetPlayerRoomRequestRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aSetPlayerRoomRequest, ok := obj.(SetPlayerRoomRequest)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertSetPlayerRoomRequestRequired(aSetPlayerRoomRequest)
	})
}
