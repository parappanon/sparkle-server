/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type GetAllPlayerQuestResponse struct {
	CharacterQuests []CharacterQuestsArrayObject `json:"characterQuests"`

	EventQuestPeriods []EventQuestPeriodsArrayObject `json:"eventQuestPeriods"`

	EventQuests []EventQuestsArrayObject `json:"eventQuests"`

	LastPlayedChapterQuestIds []int64 `json:"lastPlayedChapterQuestIds"`

	// Player's unread new achievement count (most useless value)
	NewAchievementCount int32 `json:"newAchievementCount"`

	PlayedOpenChapterIds []int64 `json:"playedOpenChapterIds"`

	PlayerOfferQuests []PlayerOfferQuestsArrayObject `json:"playerOfferQuests"`

	QuestPart2s []Questpart2sArrayObject `json:"questPart2s"`

	QuestStaminaReductions []QuestStaminaReductionsArrayObject `json:"questStaminaReductions"`

	Quests []QuestsArrayObject `json:"quests"`

	ReadGroups []interface{} `json:"readGroups"`

	// Generic server resultCode response type (Ex. 0)
	ResultCode int32 `json:"resultCode"`

	// Generic server resultMessage type (Ex. SUCCESS)
	ResultMessage string `json:"resultMessage"`

	// Generic server time response type (Ex. 2023-01-20T00:49:57)
	ServerTime string `json:"serverTime"`

	// Generic server version response type (Ex. 2211181)
	ServerVersion int32 `json:"serverVersion"`
}

// AssertGetAllPlayerQuestResponseRequired checks if the required fields are not zero-ed
func AssertGetAllPlayerQuestResponseRequired(obj GetAllPlayerQuestResponse) error {
	elements := map[string]interface{}{
		"characterQuests":           obj.CharacterQuests,
		"eventQuestPeriods":         obj.EventQuestPeriods,
		"eventQuests":               obj.EventQuests,
		"lastPlayedChapterQuestIds": obj.LastPlayedChapterQuestIds,
		"newAchievementCount":       obj.NewAchievementCount,
		"playedOpenChapterIds":      obj.PlayedOpenChapterIds,
		"playerOfferQuests":         obj.PlayerOfferQuests,
		"questPart2s":               obj.QuestPart2s,
		"questStaminaReductions":    obj.QuestStaminaReductions,
		"quests":                    obj.Quests,
		"readGroups":                obj.ReadGroups,
		"resultCode":                obj.ResultCode,
		"resultMessage":             obj.ResultMessage,
		"serverTime":                obj.ServerTime,
		"serverVersion":             obj.ServerVersion,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	for _, el := range obj.CharacterQuests {
		if err := AssertCharacterQuestsArrayObjectRequired(el); err != nil {
			return err
		}
	}
	for _, el := range obj.EventQuestPeriods {
		if err := AssertEventQuestPeriodsArrayObjectRequired(el); err != nil {
			return err
		}
	}
	for _, el := range obj.EventQuests {
		if err := AssertEventQuestsArrayObjectRequired(el); err != nil {
			return err
		}
	}
	for _, el := range obj.PlayerOfferQuests {
		if err := AssertPlayerOfferQuestsArrayObjectRequired(el); err != nil {
			return err
		}
	}
	for _, el := range obj.QuestPart2s {
		if err := AssertQuestpart2sArrayObjectRequired(el); err != nil {
			return err
		}
	}
	for _, el := range obj.QuestStaminaReductions {
		if err := AssertQuestStaminaReductionsArrayObjectRequired(el); err != nil {
			return err
		}
	}
	for _, el := range obj.Quests {
		if err := AssertQuestsArrayObjectRequired(el); err != nil {
			return err
		}
	}
	return nil
}

// AssertRecurseGetAllPlayerQuestResponseRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of GetAllPlayerQuestResponse (e.g. [][]GetAllPlayerQuestResponse), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseGetAllPlayerQuestResponseRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aGetAllPlayerQuestResponse, ok := obj.(GetAllPlayerQuestResponse)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertGetAllPlayerQuestResponseRequired(aGetAllPlayerQuestResponse)
	})
}
