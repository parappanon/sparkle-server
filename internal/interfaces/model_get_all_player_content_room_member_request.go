/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type GetAllPlayerContentRoomMemberRequest struct {
	TitleType int64 `json:"titleType"`
}

// AssertGetAllPlayerContentRoomMemberRequestRequired checks if the required fields are not zero-ed
func AssertGetAllPlayerContentRoomMemberRequestRequired(obj GetAllPlayerContentRoomMemberRequest) error {
	elements := map[string]interface{}{
		"titleType": obj.TitleType,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseGetAllPlayerContentRoomMemberRequestRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of GetAllPlayerContentRoomMemberRequest (e.g. [][]GetAllPlayerContentRoomMemberRequest), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseGetAllPlayerContentRoomMemberRequestRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aGetAllPlayerContentRoomMemberRequest, ok := obj.(GetAllPlayerContentRoomMemberRequest)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertGetAllPlayerContentRoomMemberRequestRequired(aGetAllPlayerContentRoomMemberRequest)
	})
}
