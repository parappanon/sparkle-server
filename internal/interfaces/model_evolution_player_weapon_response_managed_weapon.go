/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type EvolutionPlayerWeaponResponseManagedWeapon struct {
	Exp int64 `json:"exp"`

	Level int64 `json:"level"`

	ManagedWeaponId int64 `json:"managedWeaponId"`

	SkillExp int64 `json:"skillExp"`

	SkillLevel int64 `json:"skillLevel"`

	SoldAt string `json:"soldAt"`

	State int64 `json:"state"`

	WeaponId int64 `json:"weaponId"`
}

// AssertEvolutionPlayerWeaponResponseManagedWeaponRequired checks if the required fields are not zero-ed
func AssertEvolutionPlayerWeaponResponseManagedWeaponRequired(obj EvolutionPlayerWeaponResponseManagedWeapon) error {
	elements := map[string]interface{}{
		"exp":             obj.Exp,
		"level":           obj.Level,
		"managedWeaponId": obj.ManagedWeaponId,
		"skillExp":        obj.SkillExp,
		"skillLevel":      obj.SkillLevel,
		"soldAt":          obj.SoldAt,
		"state":           obj.State,
		"weaponId":        obj.WeaponId,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseEvolutionPlayerWeaponResponseManagedWeaponRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of EvolutionPlayerWeaponResponseManagedWeapon (e.g. [][]EvolutionPlayerWeaponResponseManagedWeapon), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseEvolutionPlayerWeaponResponseManagedWeaponRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aEvolutionPlayerWeaponResponseManagedWeapon, ok := obj.(EvolutionPlayerWeaponResponseManagedWeapon)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertEvolutionPlayerWeaponResponseManagedWeaponRequired(aEvolutionPlayerWeaponResponseManagedWeapon)
	})
}
