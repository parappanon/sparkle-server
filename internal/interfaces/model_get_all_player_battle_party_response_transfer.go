package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toGetAllPlayerBattlePartyResponse(userBattleParties *[]model_user.ManagedBattleParty, success *response.BaseResponse) *GetAllPlayerBattlePartyResponse {
	managedBattleParties := make([]ManagedBattlePartiesArrayObject, 0)
	for _, obj := range *userBattleParties {
		managedBattleParties = append(managedBattleParties, ManagedBattlePartiesArrayObject{
			CostLimit:            int64(obj.CostLimit),
			ManagedBattlePartyId: int64(obj.ManagedBattlePartyId),
			ManagedCharacterId1:  int64(obj.ManagedCharacterId1),
			ManagedCharacterId2:  int64(obj.ManagedCharacterId2),
			ManagedCharacterId3:  int64(obj.ManagedCharacterId3),
			ManagedCharacterId4:  int64(obj.ManagedCharacterId4),
			ManagedCharacterId5:  int64(obj.ManagedCharacterId5),
			ManagedCharacterIds: []int64{
				obj.ManagedCharacterId1,
				obj.ManagedCharacterId2,
				obj.ManagedCharacterId3,
				obj.ManagedCharacterId4,
				obj.ManagedCharacterId5,
			},
			ManagedWeaponId1: int64(obj.ManagedWeaponId1),
			ManagedWeaponId2: int64(obj.ManagedWeaponId2),
			ManagedWeaponId3: int64(obj.ManagedWeaponId3),
			ManagedWeaponId4: int64(obj.ManagedWeaponId4),
			ManagedWeaponId5: int64(obj.ManagedWeaponId5),
			ManagedWeaponIds: []int64{
				int64(obj.ManagedWeaponId1),
				int64(obj.ManagedWeaponId2),
				int64(obj.ManagedWeaponId3),
				int64(obj.ManagedWeaponId4),
				int64(obj.ManagedWeaponId5),
			},
			MasterOrbId: int64(obj.MasterOrbId),
			Name:        obj.Name,
		})
	}

	return &GetAllPlayerBattlePartyResponse{
		ManagedBattleParties: managedBattleParties,
		NewAchievementCount:  success.NewAchievementCount,
		ResultCode:           success.ResultCode,
		ResultMessage:        success.ResultMessage,
		ServerTime:           success.ServerTime,
		ServerVersion:        success.ServerVersion,
	}
}
