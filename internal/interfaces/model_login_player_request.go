/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type LoginPlayerRequest struct {

	// UUID like string
	AccessToken string `json:"accessToken"`

	// App version (3.2.17)
	AppVersion string `json:"appVersion"`

	// Unknown but 0 in almost cases
	AuthVoided int64 `json:"authVoided"`

	// 1: iOS / 2: Android
	Platform int64 `json:"platform"`

	// UUID like string
	Uuid string `json:"uuid"`
}

// AssertLoginPlayerRequestRequired checks if the required fields are not zero-ed
func AssertLoginPlayerRequestRequired(obj LoginPlayerRequest) error {
	elements := map[string]interface{}{
		"accessToken": obj.AccessToken,
		"appVersion":  obj.AppVersion,
		"platform":    obj.Platform,
		"uuid":        obj.Uuid,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseLoginPlayerRequestRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of LoginPlayerRequest (e.g. [][]LoginPlayerRequest), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseLoginPlayerRequestRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aLoginPlayerRequest, ok := obj.(LoginPlayerRequest)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertLoginPlayerRequestRequired(aLoginPlayerRequest)
	})
}
