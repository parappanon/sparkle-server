/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type BuySetPlayerRoomObjectResponse struct {
	ManagedRoomObjectIds string `json:"managedRoomObjectIds"`

	ManagedRoomObjects []ManagedRoomObjectsArrayObject `json:"managedRoomObjects"`

	ManagedRooms []ManagedRoomsArrayObject `json:"managedRooms"`

	// Player's unread new achievement count (most useless value)
	NewAchievementCount int32 `json:"newAchievementCount"`

	Player BasePlayer `json:"player"`

	// Generic server resultCode response type (Ex. 0)
	ResultCode int32 `json:"resultCode"`

	// Generic server resultMessage type (Ex. SUCCESS)
	ResultMessage string `json:"resultMessage"`

	// Generic server time response type (Ex. 2023-01-20T00:49:57)
	ServerTime string `json:"serverTime"`

	// Generic server version response type (Ex. 2211181)
	ServerVersion int32 `json:"serverVersion"`
}

// AssertBuySetPlayerRoomObjectResponseRequired checks if the required fields are not zero-ed
func AssertBuySetPlayerRoomObjectResponseRequired(obj BuySetPlayerRoomObjectResponse) error {
	elements := map[string]interface{}{
		"managedRoomObjectIds": obj.ManagedRoomObjectIds,
		"managedRoomObjects":   obj.ManagedRoomObjects,
		"managedRooms":         obj.ManagedRooms,
		"newAchievementCount":  obj.NewAchievementCount,
		"player":               obj.Player,
		"resultCode":           obj.ResultCode,
		"resultMessage":        obj.ResultMessage,
		"serverTime":           obj.ServerTime,
		"serverVersion":        obj.ServerVersion,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	for _, el := range obj.ManagedRoomObjects {
		if err := AssertManagedRoomObjectsArrayObjectRequired(el); err != nil {
			return err
		}
	}
	for _, el := range obj.ManagedRooms {
		if err := AssertManagedRoomsArrayObjectRequired(el); err != nil {
			return err
		}
	}
	if err := AssertBasePlayerRequired(obj.Player); err != nil {
		return err
	}
	return nil
}

// AssertRecurseBuySetPlayerRoomObjectResponseRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of BuySetPlayerRoomObjectResponse (e.g. [][]BuySetPlayerRoomObjectResponse), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseBuySetPlayerRoomObjectResponseRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aBuySetPlayerRoomObjectResponse, ok := obj.(BuySetPlayerRoomObjectResponse)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertBuySetPlayerRoomObjectResponseRequired(aBuySetPlayerRoomObjectResponse)
	})
}
