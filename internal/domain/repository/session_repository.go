package repository

type SessionRepository interface {
	Validate(session string) (int32, error)
}
