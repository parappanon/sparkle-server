package repository

import (
	model_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/model/town_facility"
)

type TownFacilityRepository interface {
	FindTownFacility(facilityId uint32) (*model_town_facility.TownFacility, error)
}
