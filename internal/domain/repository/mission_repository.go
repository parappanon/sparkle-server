package repository

import (
	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
)

type MissionRepository interface {
	FindMission(query *model_mission.Mission, criteria map[string]interface{}, associations []string) (*model_mission.Mission, error)
	FindMissions(query *model_mission.Mission, criteria map[string]interface{}, associations []string) ([]*model_mission.Mission, error)
}
