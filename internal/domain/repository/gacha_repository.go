package repository

import (
	model_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/model/gacha"
)

type GachaRepository interface {
	FindGacha(query *model_gacha.Gacha, criteria map[string]interface{}, associations *[]string) (*model_gacha.Gacha, error)
	FindGachas(query *model_gacha.Gacha, criteria map[string]interface{}, associations *[]string) ([]*model_gacha.Gacha, error)
}
