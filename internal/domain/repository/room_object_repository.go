package repository

import (
	model_room_object "gitlab.com/kirafan/sparkle/server/internal/domain/model/room_object"
)

type RoomObjectRepository interface {
	FindRoomObject(roomObjectId uint) (*model_room_object.RoomObject, error)
	FindRoomObjects(query *model_room_object.RoomObject, criteria map[string]interface{}, associations *[]string) ([]*model_room_object.RoomObject, error)
}
