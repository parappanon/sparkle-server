package repository

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
)

type ExpTableSkillRepository interface {
	FindExpTableSkill(query *model_exp_table.ExpTableSkill, criteria map[string]interface{}) (*model_exp_table.ExpTableSkill, error)
	FindExpTableSkills(query *model_exp_table.ExpTableSkill, criteria map[string]interface{}) ([]*model_exp_table.ExpTableSkill, error)
}
