package repository

import (
	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
)

type EvoTableLimitBreakRepository interface {
	FindByRecipeId(recipeId uint) (*model_evo_table.EvoTableLimitBreak, error)
}
