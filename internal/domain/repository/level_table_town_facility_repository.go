package repository

import (
	model_level_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/level_table"
)

type LevelTableTownFacilityRepository interface {
	FindLevelTableTownFacility(query *model_level_table.LevelTableTownFacility) (*model_level_table.LevelTableTownFacility, error)
}
