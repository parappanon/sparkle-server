package repository

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
)

type QuestRepository interface {
	FindQuest(query *model_quest.Quest, criteria map[string]interface{}, associations []string) (*model_quest.Quest, error)
	FindQuests(query *model_quest.Quest, criteria map[string]interface{}, associations []string) ([]*model_quest.Quest, error)
}
