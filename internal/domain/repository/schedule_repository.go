package repository

import (
	model_schedule "gitlab.com/kirafan/sparkle/server/internal/domain/model/schedule"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type ScheduleRepository interface {
	Create(characterIds []value_character.CharacterId, gridData string) ([]*model_schedule.Schedule, error)
	Refresh(characterSchedules []string) ([]string, error)
}
