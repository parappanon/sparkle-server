package repository

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
)

type UserRepositoryParam struct {
	Entire                   bool
	ItemSummary              bool
	SupportCharacters        bool
	ManagedBattleParties     bool
	ManagedCharacters        bool
	ManagedNamedTypes        bool
	ManagedFieldPartyMembers bool
	ManagedTowns             bool
	ManagedFacilities        bool
	ManagedRoomObjects       bool
	ManagedWeapons           bool
	ManagedRooms             bool
	ManagedMasterOrbs        bool
	ManagedAbilityBoards     bool
	FavoriteMembers          bool
	OfferTitleTypes          bool
	AdvIds                   bool
	TipIds                   bool
	QuestLogs                bool
	QuestAccesses            bool
	Missions                 bool
	Presents                 bool
	Gachas                   bool
	LoginBonuses             bool
	Achievements             bool
}

type UserRepository interface {
	CreateUser(user *model_user.User) (*model_user.User, error)
	FindUserById(id uint, param UserRepositoryParam) (*model_user.User, error)
	FindUserByUUID(UUId string, param UserRepositoryParam) (*model_user.User, error)
	FindUserBySession(deviceUUId string, sessionUUId string) (*model_user.User, error)
	FindUserByFriendCode(friendCode string) (*model_user.User, error)
	FindUserByMoveCode(moveCode string) (*model_user.User, error)
	UpdateUser(user *model_user.User, param UserRepositoryParam) (*model_user.User, error)
	InsertUserQuestLog(userId uint, quest *model_user.QuestLog) (*model_user.QuestLog, error)
	UpdateUserQuestLog(quest *model_user.QuestLog) (*model_user.QuestLog, error)
	FindUserQuestLog(userId uint, query *model_user.QuestLog, order *string) (*model_user.QuestLog, error)
	GetUserQuestLogClearRanks(userId uint) ([]*model_user.QuestClearRank, error)
}
