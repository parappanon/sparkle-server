package model_user

type ManagedRoomObject struct {
	ManagedRoomObjectId int `gorm:"primarykey"`
	// Foreign key
	UserId       uint
	RoomObjectId uint32
}
