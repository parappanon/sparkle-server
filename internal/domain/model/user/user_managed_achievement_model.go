package model_user

import (
	"time"

	model_achievement "gitlab.com/kirafan/sparkle/server/internal/domain/model/achievement"
)

// Moved from achievement model, these values depends per a user
type UserAchievement struct {
	ManagedAchievementId uint `gorm:"primary_key"`
	CreatedAt            time.Time
	UpdatedAt            time.Time

	Enable bool
	IsNew  bool

	// foreignKey
	AchievementId uint
	Achievement   model_achievement.Achievement `gorm:"PRELOAD:false"`
	// foreignKey
	UserId uint
	User   User `gorm:"PRELOAD:false"`
}
