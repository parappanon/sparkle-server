package model_user

import "gorm.io/gorm"

// Means ManagedSupportParty
type SupportCharacter struct {
	ManagedSupportId    int                                  `gorm:"primarykey"`
	ManagedCharacterIds []SupportCharacterManagedCharacterId `gorm:"foreignKey:ManagedSupportId"`
	ManagedWeaponIds    []SupportCharacterManagedWeaponId    `gorm:"foreignKey:ManagedSupportId"`
	// Support party name
	Name string
	// 1: Active, 0: Inactive
	Active uint8
	// Foreign Key
	UserId uint
}

type SupportCharacterManagedCharacterId struct {
	gorm.Model
	// Foreign Key (TODO: Add the key to migration)
	ManagedCharacterId int
	// Foreign Key
	ManagedSupportId int
}

type SupportCharacterManagedWeaponId struct {
	gorm.Model
	// Foreign Key (TODO: Add the key to migration)
	ManagedWeaponId int
	// Foreign Key
	ManagedSupportId int
}
