package model_user

func newManagedWeapon(userId uint, weaponId uint32) ManagedWeapon {
	return ManagedWeapon{
		WeaponId:   weaponId,
		Level:      1,
		Exp:        0,
		SkillLevel: 1,
		SkillExp:   0,
		State:      1,
		SoldAt:     nil,
	}
}
