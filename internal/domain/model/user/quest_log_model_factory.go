package model_user

import (
	"errors"
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
)

var ErrNewQuestLogInvalidManagedBattlePartyId = errors.New("invalid managed battle party id")

func NewQuestLogForAdvOnly(
	user User,
	questId uint,
) QuestLog {
	now := time.Now()
	questLog := QuestLog{
		UserId:      user.Id,
		QuestId:     questId,
		CreatedAt:   now,
		UpdatedAt:   now,
		QuestData:   "",
		WaveData:    nil,
		DropData:    nil,
		ClearRank:   value_quest.ClearRankGold,
		IsRead:      value.BoolLikeUIntTrue,
		CurrentWave: 0,
		TotalWave:   0,
		Characters:  []QuestLogCharacter{},
		Support:     nil,
	}
	return questLog
}

func NewQuestLog(
	user User,
	managedBattlePartyId uint,
	questId uint,
	questData string,
	waveData *string,
	dropData *string,
	waveCount uint8,
) (QuestLog, error) {
	now := time.Now()
	var managedBattleParty *ManagedBattleParty
	for _, battleParty := range user.ManagedBattleParties {
		if uint(battleParty.ManagedBattlePartyId) == managedBattlePartyId {
			managedBattleParty = &battleParty
			break
		}
	}
	if managedBattleParty == nil {
		return QuestLog{}, ErrNewQuestLogInvalidManagedBattlePartyId
	}

	questLog := QuestLog{
		UserId:      user.Id,
		QuestId:     questId,
		CreatedAt:   now,
		UpdatedAt:   now,
		QuestData:   questData,
		WaveData:    waveData,
		DropData:    dropData,
		ClearRank:   value_quest.ClearRankNone,
		IsRead:      0,
		CurrentWave: 0,
		TotalWave:   waveCount,
		Characters:  []QuestLogCharacter{},
		Support:     nil,
	}
	return questLog, nil
}
