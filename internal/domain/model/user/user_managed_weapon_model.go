package model_user

import (
	"time"

	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type ManagedWeapon struct {
	ManagedWeaponId int `gorm:"primarykey"`
	// Foreign key
	UserId uint
	// WeaponId (character specific weapon has same Id as characterId, so 16 is not enough)
	WeaponId   uint32
	Level      uint8  `json:"level"`
	Exp        uint32 `json:"exp"`
	SkillLevel uint8  `json:"skillLevel"`
	SkillExp   uint32 `json:"skillExp"`
	// Unknown static value (default: 1)
	State uint8 `json:"state"`
	// Unknown static value (default: 0001-01-01T00:00:00)
	SoldAt *time.Time
}

func (w *ManagedWeapon) AddWeaponLevel(level uint8) {
	w.Level += level
}

func (w *ManagedWeapon) AddWeaponExp(exp uint32) {
	w.Exp += exp
}

func (w *ManagedWeapon) UpdateWeaponSkillLevel(level uint8) {
	w.SkillLevel = calc.Max(w.SkillLevel, level)
}

func (w *ManagedWeapon) AddWeaponSkillExp(exp uint32) {
	w.SkillExp += exp
}
