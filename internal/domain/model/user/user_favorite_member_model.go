package model_user

import (
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gorm.io/gorm"
)

// Favorites at player card
type FavoriteMember struct {
	gorm.Model
	// Foreign key
	UserId uint
	// Foreign key
	ManagedCharacterId value_user.ManagedCharacterId
	FavoriteIndex      uint8                       `json:"favoriteIndex"`
	CharacterId        value_character.CharacterId `json:"characterId"`
	ArousalLevel       uint8                       `json:"arousalLevel"`
}
