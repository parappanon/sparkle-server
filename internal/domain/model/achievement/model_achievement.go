package model_achievement

import (
	"time"

	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type Achievement struct {
	Id        uint  `gorm:"primary_key"`
	SortId    int64 `json:"sortId"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
	TitleType value_character.TitleType

	Name        string
	Description string
}
