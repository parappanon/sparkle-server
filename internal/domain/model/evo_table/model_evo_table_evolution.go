package model_evo_table

import (
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_evolution "gitlab.com/kirafan/sparkle/server/internal/domain/value/evolution"
)

type EvoTableEvolution struct {
	RecipeId       uint `gorm:"primaryKey"`
	RequiredCoin   uint32
	RecipeType     value_evolution.EvolutionRecipeType
	SrcCharacterId value_character.CharacterId
	DstCharacterId value_character.CharacterId
	RequiredItems  []EvoTableEvolutionRequiredItem `gorm:"foreignKey:RecipeId"`
}
