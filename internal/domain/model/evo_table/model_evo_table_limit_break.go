package model_evo_table

import (
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type EvoTableLimitBreak struct {
	RecipeId            uint `gorm:"primaryKey"`
	Rarity              value_character.CharacterRarity
	Class               value_character.ClassType
	RequiredCoinPerItem uint32
	ClassItemId         int32
	ClassItemAmount     int16
	AllClassItemId      int32
	AllClassItemAmount  int16
	TitleItemAmount     int16
}
