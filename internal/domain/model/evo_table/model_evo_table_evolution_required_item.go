package model_evo_table

type EvoTableEvolutionRequiredItem struct {
	RecipeRequiredItemId uint `gorm:"primaryKey"`
	// Foreign key
	RecipeId uint
	ItemId   int32
	Amount   int16
}
