package model_gacha

type GachaStepBonusItem struct {
	GachaStepBonusItemId uint `gorm:"primary_key"`
	Id                   uint32
	Amount               uint64
	// Foreign key
	GachaStepId uint
}
