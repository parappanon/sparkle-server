package model_gacha

import (
	"time"

	value_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/value/gacha"
)

type GachaDailyFree struct {
	GachaDailyFreeId uint `gorm:"primary_key"`
	GachaId          uint32
	DrawType         value_gacha.GachaDrawType
	StartAt          time.Time
	EndAt            time.Time
}
