package model

type EventQuestsArrayObjectEventquest struct {
	ChestId int64

	CondEventQuestIds []interface{}

	Day int64

	EndAt string

	EventType int64

	ExAlchemist int64

	ExCost int64

	ExEarth int64

	ExFighter int64

	ExFire int64

	ExKnight int64

	ExMagician int64

	ExMoon int64

	ExName int64

	ExPriest int64

	ExRarity int64

	ExSun int64

	ExTitle int64

	ExWater int64

	ExWind int64

	Freq int64

	Fri int64

	GroupId int64

	GroupName string

	GroupSkip int64

	Id int64

	Interval int64

	MapFlg int64

	Mon int64

	Month int64

	OrderLimit int64

	Quest EventQuestsArrayObjectEventquestQuest

	Sat int64

	StartAt string

	Sun int64

	Thu int64

	TradeGroupId int64

	Tue int64

	Wed int64
}
