package model

type EventQuestPeriodsArrayObject struct {
	EndAt string

	EventQuestPeriodGroups []EventQuestPeriodGroupsArrayObject

	EventType int64

	LossTimeEndAt *string

	StartAt string
}
