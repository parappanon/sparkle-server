package model

type ReceivedArrayObject struct {
	Amount int64

	CreatedAt string

	DeadlineAt string

	DeadlineFlag bool

	ManagedPresentId int64

	Message string

	ObjectId int64

	Options *string

	PlayerId int64

	ReceivedAt string

	Source int64

	Title string

	Type int64
}
