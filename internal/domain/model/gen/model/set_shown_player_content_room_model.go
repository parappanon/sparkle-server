package model

type SetShownPlayerContentRoomResponse struct {
	OfferTitleTypes []OfferTitleTypesArrayObject
}
