package model

type TownFacilityBuyStatesArrayObject struct {
	ActionNo int64

	ActionTime int64

	BuildPointIndex int64

	BuildTime int64

	FacilityId int64

	NextLevel int64

	OpenState int64
}
