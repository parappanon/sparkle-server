package model

type EquipPlayerAbilityResponse struct {
	ItemSummary []ItemSummaryArrayObject

	PlayerAbilityBoards []PlayerAbilityBoardsArrayObject
}
