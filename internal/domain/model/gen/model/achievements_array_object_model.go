package model

type AchievementsArrayObject struct {
	Description string

	Enable int64

	Id int64

	IsNew int64

	Name string

	SortId int64

	TitleType int64
}
