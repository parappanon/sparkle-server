package model

type LimitbreakPlayerCharacterResponse struct {
	Gold int64

	ItemSummary []ItemSummaryArrayObject

	ManagedCharacter LimitbreakPlayerCharacterResponseManagedCharacter
}
