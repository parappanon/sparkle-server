package model

type MembersArrayObject2 struct {
	Flag int64

	ManagedCharacterId int64

	ManagedFacilityId int64

	ManagedPartyMemberId int64

	ScheduleId int64

	ScheduleTable string

	ScheduleTag int64
}
