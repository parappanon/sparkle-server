package model

type GetPlayerMoveResponse struct {
	MoveCode string

	MoveDeadline string
}
