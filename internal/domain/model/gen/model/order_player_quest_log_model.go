package model

type OrderPlayerQuestLogResponse struct {
	DropItem []DropItemArrayObject

	ItemSummary []ItemSummaryArrayObject

	OccurEnemy OrderPlayerQuestLogResponseOccurEnemy

	OrderReceiveId int64

	Player BasePlayer
}
