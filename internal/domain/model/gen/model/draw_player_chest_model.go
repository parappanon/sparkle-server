package model

type DrawPlayerChestResponse struct {
	AddPresentAmount int64

	Chest DrawPlayerChestResponseChest

	ItemSummary []ItemSummaryArrayObject

	ManagedRoomObjects []ManagedRoomObjectsArrayObject

	ManagedWeapons []ManagedWeaponsArrayObject

	Player BasePlayer

	PrizeResults []PrizeResultsArrayObject
}
