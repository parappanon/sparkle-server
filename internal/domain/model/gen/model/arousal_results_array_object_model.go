package model

type ArousalResultsArrayObject struct {
	ArousalItemAmount int64

	ArousalItemId int64

	ArousalLevel int64

	CharacterId int64

	DuplicatedCount int64
}
