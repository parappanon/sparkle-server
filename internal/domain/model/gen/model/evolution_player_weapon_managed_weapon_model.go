package model

type EvolutionPlayerWeaponResponseManagedWeapon struct {
	Exp int64

	Level int64

	ManagedWeaponId int64

	SkillExp int64

	SkillLevel int64

	SoldAt string

	State int64

	WeaponId int64
}
