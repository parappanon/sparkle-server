package model

type GetPlayerResumeResponse struct {
	RecastTime int64

	RecastTimeMax int64

	Stamina int64

	StaminaMax int64
}
