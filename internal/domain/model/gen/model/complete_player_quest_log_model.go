package model

type CompletePlayerQuestLogResponse struct {
	IsContinuous int64

	IsFirstComplete int64

	ItemSummary []ItemSummaryArrayObject

	ManagedCharacters []ManagedCharactersArrayObject

	ManagedNamedTypes []ManagedNamedTypesArrayObject

	ManagedWeapons []ManagedWeaponsArrayObject

	OfferTitleTypes []OfferTitleTypesArrayObject

	Offers []interface{}

	Player BasePlayer

	PointEventResult *string

	QuestNoticeMessage *string

	QuestNoticeTitle *string

	RewardComp *string

	RewardFirst *string

	RewardGroup *string
}
