package model

type BattlePartyMembersArrayObject struct {
	ManagedBattlePartyId int64

	ManagedCharacterIds []int64

	ManagedWeaponIds []int64

	MasterOrbId int64
}
