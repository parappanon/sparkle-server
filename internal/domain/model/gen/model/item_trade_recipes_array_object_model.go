package model

type ItemTradeRecipesArrayObject struct {
	AltAmount int64

	AltId int64

	AltMessageMakeId int64

	AltType int64

	DstAmount int64

	DstId int64

	DstType int64

	EndAt *string

	EventType int64

	GroupId int64

	Id int64

	LabelId int64

	LimitCount int64

	LimitedFlag int64

	MessageMakeId int64

	MonthlyTradeCount int64

	ResetFlag int64

	Sort int64

	SrcAmount1 int64

	SrcAmount2 int64

	SrcId1 int64

	SrcId2 int64

	SrcType1 int64

	SrcType2 int64

	StartAt string

	TotalTradeCount int64

	TradeMax int64
}
