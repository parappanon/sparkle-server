package model

type SupportCharactersArrayObject struct {
	Active int64 `json:"active,omitempty"`

	ManagedCharacterIds []int64 `json:"managedCharacterIds,omitempty"`

	ManagedSupportId int64 `json:"managedSupportId,omitempty"`

	ManagedWeaponIds []int64 `json:"managedWeaponIds,omitempty"`

	Name string `json:"name,omitempty"`
}
