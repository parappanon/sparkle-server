package model

type GetPlayerGachaBoxResponse struct {
	Characters []int64

	Pickups []interface{}
}
