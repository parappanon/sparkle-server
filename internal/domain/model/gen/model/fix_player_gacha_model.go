package model

type FixPlayerGachaResponse struct {
	ArousalResults []ArousalResultsArrayObject

	ItemSummary []ItemSummaryArrayObject

	ManagedCharacters []ManagedCharactersArrayObject

	ManagedNamedTypes []ManagedNamedTypesArrayObject

	OfferTitleTypes []OfferTitleTypesArrayObject

	Offers []interface{}
}
