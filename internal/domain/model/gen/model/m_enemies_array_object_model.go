package model

type MEnemiesArrayObject struct {
	MDropId int64

	MEnemyId int64

	MEnemyLv int64

	MScale float32

	Point int64
}
