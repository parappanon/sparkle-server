package model

type BuySetPlayerRoomObjectResponse struct {
	ManagedRoomObjectIds string

	ManagedRoomObjects []ManagedRoomObjectsArrayObject

	ManagedRooms []ManagedRoomsArrayObject

	Player BasePlayer
}
