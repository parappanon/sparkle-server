package model

type UpgradePlayerCharacterResponse struct {
	Gold int64

	ItemSummary []ItemSummaryArrayObject

	ManagedCharacter UpgradePlayerCharacterResponseManagedCharacter

	SuccessType int64
}
