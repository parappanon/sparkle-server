package model_quest

import (
	"time"

	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
)

type QuestStaminaReduction struct {
	Id            uint `gorm:"primaryKey"`
	QuestCategory value_quest.QuestCategoryType
	Difficulty    uint8
	TargetId      uint64
	StartAt       time.Time
	EndAt         time.Time
}
