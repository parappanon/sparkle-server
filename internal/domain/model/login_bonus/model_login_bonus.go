package model_login_bonus

import (
	"time"

	value_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/value/login_bonus"
)

type LoginBonus struct {
	BonusId   uint            `gorm:"primaryKey"`
	BonusDays []LoginBonusDay `gorm:"foreignKey:BonusId"`
	ImageId   value_login_bonus.LoginBonusImageId
	Type      value_login_bonus.LoginBonusType
	// Internal request accept time range
	StartAt time.Time
	EndAt   time.Time
}
