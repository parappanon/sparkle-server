package model_exp_table

import (
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
)

type ExpTableSkill struct {
	ExpTableSkillId uint `gorm:"primaryKey"`
	SkillType       value_exp.ExpTableSkillType
	Level           uint8
	NextExp         uint32
	TotalExp        uint32
}
