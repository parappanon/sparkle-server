package model_exp_table

import (
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
)

type ExpTableWeapon struct {
	ExpTableWeaponId    uint `gorm:"primaryKey"`
	WeaponType          value_exp.ExpTableWeaponType
	Level               uint16
	NextExp             uint32
	TotalExp            uint32
	RequiredCoinPerItem uint16
}
