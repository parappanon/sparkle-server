package model_mission

import (
	value_mission "gitlab.com/kirafan/sparkle/server/internal/domain/value/mission"
)

type MissionReward struct {
	Id uint `gorm:"primary_key"`
	// Foreign key
	MissionId  uint
	RewardType value_mission.MissionRewardType
	// ItemId or -1
	RewardNo int64
	// Reward count to obtain (must be big because of coin is 64bit)
	RewardNum int64
}
