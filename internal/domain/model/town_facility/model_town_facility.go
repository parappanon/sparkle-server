package model_town_facility

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
)

type TownFacility struct {
	// NOTE: It accepts nil Since facilityId can be 0
	// https://stackoverflow.com/questions/54523765/update-with-0-value-using-gorm
	FacilityId       *uint32 `gorm:"primary_key"`
	FacilityCategory value_town_facility.FacilityCategory
	Name             string
	Detail           string
	MaxLevel         uint8
	MaxNum           uint8
	TitleType        value_character.TitleType
	ClassType        value_character.ClassType
	BuildTimeType    value_town_facility.FacilityBuildTimeType
	LevelUpListId    value_town_facility.LevelUpListId
	IsTutorial       value.BoolLikeUInt8
}
