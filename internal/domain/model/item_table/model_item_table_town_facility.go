package model_item_table

import (
	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
)

type ItemTableTownFacility struct {
	LevelTableFacilityID uint `gorm:"primary_key"`
	ItemNo               uint32
	Category             value_town_facility.ItemTableTownFacilityCategory
	ObjectID             int64
	ObjectCount          uint32
}
