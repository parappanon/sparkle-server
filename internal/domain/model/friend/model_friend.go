package model_friend

import (
	"time"

	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
)

type Friend struct {
	// DB primary key
	ManagedFriendId uint
	// Foreign key
	// FIXME: Change to value object
	PlayerId  uint
	State     value_friend.FriendState
	Direction value_friend.FriendDirection
	Name      string
	MyCode    string
	Comment   string
	// FIXME: Change to value object
	CurrentAchievementId int64
	// FIXME: Change to value object
	Level       uint16
	LastLoginAt time.Time
	// Dead parameter (always 0)
	TotalExp uint8
	// Is this value needed...?
	SupportLimit uint8
	// Dead parameter (always nil)
	NamedTypes        *string
	SupportName       string
	SupportCharacters []FriendSupportCharacter
	// NOTE: FirstFavoriteMember has fewer fields than FriendSupportCharacter but is otherwise identical
	// So we can use the same struct for both
	FirstFavoriteMember FriendSupportCharacter
}
