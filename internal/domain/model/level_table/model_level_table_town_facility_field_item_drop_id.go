package model_level_table

type LevelTableTownFacilityFieldItemDropId struct {
	LevelTableFacilityId *uint `gorm:"primaryKey"`
	// Index
	FieldItemDropListId uint32
	FieldItemDropId     uint32
}
