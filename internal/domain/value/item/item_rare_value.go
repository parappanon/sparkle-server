package value_item

import "errors"

type ItemRare uint8

const (
	ItemRare1 ItemRare = iota
	ItemRare2
	ItemRare3
	ItemRare4
	ItemRare5
)

var ErrInvalidItemRare = errors.New("invalid ItemRare")

func NewItemRare(v uint8) (ItemRare, error) {
	if v > uint8(ItemRare5) {
		return ItemRare1, ErrInvalidItemRare
	}
	return ItemRare(v), nil
}
