package value_item

import "errors"

type ItemCategory uint16

const (
	// Seeds
	ItemCategorySeedSmall      ItemCategory = 0
	ItemCategorySeedNormal     ItemCategory = 1
	ItemCategorySeedLarge      ItemCategory = 2
	ItemCategorySeedExtraLarge ItemCategory = 3
	// 星華 (Unused category)
	ItemCategoryContentLimitedStarflower ItemCategory = 4
	// Evolution Jewels
	ItemCategorySmallEvolutionJewel      ItemCategory = 100
	ItemCategoryNormalEvolutionJewel     ItemCategory = 101
	ItemCategoryLargeEvolutionJewel      ItemCategory = 102
	ItemCategoryExtraLargeEvolutionJewel ItemCategory = 103
	ItemCategorySuperEvolutionJewel      ItemCategory = 104
	// Statues
	ItemCategoryBronzeStatue ItemCategory = 112
	ItemCategorySilverStatue ItemCategory = 113
	ItemCategoryGoldStatue   ItemCategory = 114
	// コンテンツ限定超進化珠 (Unused category)
	ItemCategoryContentLimitedSuperEvolutionJewel ItemCategory = 124
	// Buds, fruits, and Star Crystals
	ItemCategoryLimitBreakStar3               ItemCategory = 202
	ItemCategoryLimitBreakStar4               ItemCategory = 203
	ItemCategoryLimitBreakStar5               ItemCategory = 204
	ItemCategoryLimitBreakStar5ContentLimited ItemCategory = 214
	// Symbols, crests, and weapon materials
	ItemCategoryWeaponMaterialSymbol   ItemCategory = 300
	ItemCategoryWeaponMaterialCrest    ItemCategory = 301
	ItemCategoryWeaponMaterialChapter1 ItemCategory = 311
	ItemCategoryWeaponMaterialChapter2 ItemCategory = 312
	ItemCategoryWeaponMaterialChapter3 ItemCategory = 313
	ItemCategoryWeaponMaterialChapter4 ItemCategory = 314
	ItemCategoryWeaponMaterialChapter5 ItemCategory = 315
	ItemCategoryWeaponMaterialChapter6 ItemCategory = 316
	ItemCategoryWeaponMaterialChapter7 ItemCategory = 317
	ItemCategoryWeaponMaterialChapter8 ItemCategory = 318
	// Special items, summon tickets, exchange items, stamina recovery items
	ItemCategorySpecial           ItemCategory = 404
	ItemCategorySummonTicket      ItemCategory = 405
	ItemCategoryExchangeItem      ItemCategory = 406
	ItemCategoryStaminaItem       ItemCategory = 407
	ItemCategoryEtherium          ItemCategory = 408
	ItemCategoryChanceUpKeyHolder ItemCategory = 409
	ItemCategorySkillUpPowder     ItemCategory = 410
)

var ErrInvalidItemCategory = errors.New("invalid ItemCategory")

func NewItemCategory(v uint16) (ItemCategory, error) {
	if v > uint16(ItemCategorySkillUpPowder) {
		return ItemCategorySeedSmall, ErrInvalidItemCategory
	}
	return ItemCategory(v), nil
}
