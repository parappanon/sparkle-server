package value_item

import "errors"

type ItemType uint8

const (
	ItemTypeEnhance ItemType = iota
	ItemTypeEvolve
	ItemTypeLimitBreak
	ItemTypeWeapon
	ItemTypeQuestTicket
	ItemTypeCallTicket
	ItemTypeTradable
	ItemTypeStamina
	ItemTypeEtherium
	ItemTypeChanceUpKeyHolder
	ItemTypeSkillUpPowder
	ItemTypeRetryOrCommunityCard
	ItemTypeAbilityTreeScroll
	ItemTypeAbilityTreeOpen
	ItemTypeAbilityTreeEquipment
)

var ErrInvalidItemType = errors.New("invalid ItemType")

func NewItemType(v uint8) (ItemType, error) {
	if v > uint8(ItemTypeAbilityTreeEquipment) {
		return ItemTypeEnhance, ErrInvalidItemType
	}
	return ItemType(v), nil
}
