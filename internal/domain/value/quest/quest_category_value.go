package value_quest

import "errors"

type QuestCategoryType uint8

const (
	QuestCategoryTypeMainPart1 QuestCategoryType = iota
	QuestCategoryTypeEventAuthorDay
	QuestCategoryTypeMemorialCharacter
	QuestCategoryTypeCraftQuest
	QuestCategoryTypeMainPart2
)

var ErrInvalidQuestCategoryType = errors.New("invalid quest category type")

func NewQuestCategoryType(value uint8) (QuestCategoryType, error) {
	if value > uint8(QuestCategoryTypeMainPart2) {
		return QuestCategoryType(0), ErrInvalidQuestCategoryType
	}
	return QuestCategoryType(value), nil
}
