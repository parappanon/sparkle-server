package value_quest

import "errors"

type EnemyPlacement uint8

const (
	EnemyPlacementLeft EnemyPlacement = iota
	EnemyPlacementCenter
	EnemyPlacementRight
)

var ErrInvalidEnemyPlacement = errors.New("invalid enemy placement")

func NewEnemyPlacement(value uint8) (EnemyPlacement, error) {
	if value > uint8(EnemyPlacementRight) {
		return EnemyPlacement(0), ErrInvalidEnemyPlacement
	}
	return EnemyPlacement(value), nil
}
