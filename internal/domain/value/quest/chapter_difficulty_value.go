package value_quest

import "errors"

type ChapterDifficulty uint8

const (
	ChapterDifficultyNormal ChapterDifficulty = iota
	ChapterDifficultyHard
	ChapterDifficultyStar
)

var ErrInvalidChapterDifficulty = errors.New("invalid chapter difficulty")

func NewChapterDifficulty(value uint8) (ChapterDifficulty, error) {
	if value > 2 {
		return ChapterDifficulty(0), ErrInvalidChapterPart
	}
	return ChapterDifficulty(value), nil
}
