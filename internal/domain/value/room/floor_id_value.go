package value_room

import "errors"

type FloorId uint8

const (
	FloorIdMain FloorId = iota + 1
	FloorIdSub
)

var ErrInvalidFloorId = errors.New("invalid floor id")

func NewFloorId(value uint8) (FloorId, error) {
	if value > uint8(FloorIdSub) {
		return 0, ErrInvalidFloorId
	}
	return FloorId(value), nil
}
