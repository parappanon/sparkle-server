package value_exchange_shop

type BadgeType uint8

const (
	BadgeTypeUnknown BadgeType = iota
	BadgeTypeNone
	BadgeTypeNew
	BadgeTypeUpdate
)
