package value_shared

import "errors"

type UiPriority int32

func NewUiPriority(v int32) (UiPriority, error) {
	if v < -1 || v > 9999 {
		return UiPriority(-1), errors.New("invalid UiPriority value (must be between -1 and 9999)")
	}
	return UiPriority(v), nil
}
