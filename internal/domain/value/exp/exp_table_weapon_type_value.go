package value_exp

import "errors"

type ExpTableWeaponType uint8

const (
	// 通常武器
	ExpTableWeaponTypeNormal ExpTableWeaponType = iota
	// 不明
	ExpTableWeaponTypeUnknown
	// 専用武器
	ExpTableWeaponTypeExclusive
)

var ErrInvalidExpTableWeaponType = errors.New("invalid ExpTableWeaponType")

func NewExpTableWeaponType(v uint8) (ExpTableWeaponType, error) {
	if v > uint8(ExpTableWeaponTypeExclusive) {
		return ExpTableWeaponTypeNormal, ErrInvalidExpTableWeaponType
	}
	return ExpTableWeaponType(v), nil
}
