package value_exp

import "errors"

type CharacterExp uint32

func NewCharacterExp(exp uint32) (CharacterExp, error) {
	if exp == 0 {
		return CharacterExp(0), nil
	}
	// NOTE: Minimal character exp was 10 at quest 1104300
	if exp < 10 {
		return CharacterExp(0), errors.New("too low character exp (it must be bigger than 10)")
	}
	// NOTE: Maximum character exp was 100000 at item category 4 星華
	if exp > 100000 {
		return CharacterExp(0), errors.New("too low character exp (it must be lower than 100000)")
	}
	return CharacterExp(exp), nil
}
