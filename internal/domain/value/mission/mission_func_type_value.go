package value_mission

import "errors"

type MissionFuncType uint8

const (
	MissionFuncTypeServerLoginCount           MissionFuncType = 0
	MissionFuncTypeServerLoginContinuousCount MissionFuncType = 1
	MissionFuncTypeServerBulkMissionClear     MissionFuncType = 2
	MissionFuncTypeServerUserRankUp           MissionFuncType = 3

	MissionFuncTypeQuestClearCount    MissionFuncType = 0
	MissionFuncTypeQuestClearSpecific MissionFuncType = 1

	MissionFuncTypeTownTapCharacterCount           MissionFuncType = 3
	MissionFuncTypeTownTapCharacterOrFacilityCount MissionFuncType = 7
	MissionFuncTypeTownBuildContentFacilityCount   MissionFuncType = 4
	MissionFuncTypeTownBuildEnforceFacilityCount   MissionFuncType = 5
	MissionFuncTypeTownPutContentFacilityCount     MissionFuncType = 8
	MissionFuncTypeTownPutEnforceFacilityCount     MissionFuncType = 9

	MissionFuncTypeRoomPutItemCount      MissionFuncType = 0
	MissionFuncTypeRoomTapCharacterCount MissionFuncType = 2
	MissionFuncTypeRoomVisitOtherCount   MissionFuncType = 3

	MissionFuncTypeCharacterEnhanceCreateDeck         MissionFuncType = 0
	MissionFuncTypeCharacterEnhanceCreateSupportDeck  MissionFuncType = 1
	MissionFuncTypeCharacterEnhanceEnhanceCount       MissionFuncType = 2
	MissionFuncTypeCharacterEnhanceLimitBreakCount    MissionFuncType = 3
	MissionFuncTypeCharacterEnhanceEvolutionCount     MissionFuncType = 4
	MissionFuncTypeCharacterEnhanceCreateWeaponCount  MissionFuncType = 5
	MissionFuncTypeCharacterEnhanceUpgradeWeaponCount MissionFuncType = 6
	MissionFuncTypeCharacterEnhanceEquipWeapon        MissionFuncType = 7
	MissionFuncTypeCharacterEnhanceStar4Upgrade       MissionFuncType = 10
	MissionFuncTypeCharacterEnhanceStar4LimitBreak    MissionFuncType = 11
	MissionFuncTypeCharacterEnhanceStar4Evolution     MissionFuncType = 12

	MissionFuncTypeCharacterRelationship MissionFuncType = 0
	MissionFuncTypeTrainingStartCount    MissionFuncType = 0
	MissionFuncTypeTradeCount            MissionFuncType = 0
)

var ErrInvalidMissionFuncType = errors.New("invalid MissionFuncType")

func NewMissionFuncType(v uint8) (MissionFuncType, error) {
	if v > uint8(MissionFuncTypeCharacterEnhanceStar4Evolution) {
		return MissionFuncTypeServerLoginCount, ErrInvalidMissionFuncType
	}
	return MissionFuncType(v), nil
}
