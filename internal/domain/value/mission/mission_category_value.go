package value_mission

import "errors"

type MissionCategory uint8

const (
	MissionCategoryDaily MissionCategory = iota
	MissionCategoryWeekly
	// RankUp Evolution QuestClear Beginner
	MissionCategoryTotal
	MissionCategoryEvent
	// Why character is separated...
	MissionCategoryCharacterRelationship
)

var ErrInvalidMissionCategory = errors.New("invalid MissionCategory")

func NewMissionCategory(v uint8) (MissionCategory, error) {
	if v > uint8(MissionCategoryCharacterRelationship) {
		return MissionCategoryDaily, ErrInvalidMissionCategory
	}
	return MissionCategory(v), nil
}
