package value_user

import "errors"

var ErrStepCodeInvalid = errors.New("invalid step code, it must be between -1 and 10")

// StepCode is a value for keep new account tutorial progress.
type StepCode int8

const (
	StepCodeTutorialDone StepCode = iota - 1
	StepCodeUnused0
	StepCodeAccountCreated
	StepCodeReceivedPresents
	StepCodeAdvCreaDone
	StepCodeUnused4
	StepCodeGachaDrawDone
	StepCodeAdvIntroDone
	StepCodeAdvIntro2Done
	StepCodeFirstQuestStart
	StepCodeFirstQuestDone
	StepCodeAdvIntro3Done
)

func NewStepCode(stepCode int8) (StepCode, error) {
	if stepCode < -1 || stepCode > 10 {
		return 0, ErrStepCodeInvalid
	}
	return StepCode(stepCode), nil
}
