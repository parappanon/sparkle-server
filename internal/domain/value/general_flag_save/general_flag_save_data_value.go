package value_general_flag_save

import "errors"

type GeneralFlagSaveData uint8

const (
	GeneralFlagSaveDataChapter0 GeneralFlagSaveData = iota
	GeneralFlagSaveDataChapter1
	GeneralFlagSaveDataChapter2
	GeneralFlagSaveDataChapter3
	GeneralFlagSaveDataChapter4
	GeneralFlagSaveDataChapter5
	GeneralFlagSaveDataChapter6
	GeneralFlagSaveDataChapter7
	GeneralFlagSaveDataChapter8
	GeneralFlagSaveDataChapter9
	// TODO: Check this value is sent from the client or not
	GeneralFlagSaveDataChapter10
)

var ErrInvalidGeneralFlagSaveData = errors.New("invalid general flag save data")

func NewGeneralFlagSaveData(t uint8) (GeneralFlagSaveData, error) {
	if t > uint8(GeneralFlagSaveDataChapter10) {
		return 0, ErrInvalidGeneralFlagSaveData
	}
	return GeneralFlagSaveData(t), nil
}
