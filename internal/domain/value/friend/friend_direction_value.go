package value_friend

type FriendDirection uint8

const (
	FriendDirectionGuest FriendDirection = iota
	FriendDirectionFriend
)
