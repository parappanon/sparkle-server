package value_login_bonus

import "errors"

type LoginBonusDayIndex uint8

const (
	LoginBonusDayIndexDay1 LoginBonusDayIndex = iota
	LoginBonusDayIndexDay2
	LoginBonusDayIndexDay3
	LoginBonusDayIndexDay4
	LoginBonusDayIndexDay5
	LoginBonusDayIndexDay6
	LoginBonusDayIndexDay7
	LoginBonusDayIndexDay8
	LoginBonusDayIndexDay9
	LoginBonusDayIndexDay10
	LoginBonusDayIndexDay11
	LoginBonusDayIndexDay12
	LoginBonusDayIndexDay13
	LoginBonusDayIndexDay14
)

var ErrInvalidLoginBonusDayIndex = errors.New("invalid LoginBonusDayIndex")

func NewLoginBonusDayIndex(v uint8) (LoginBonusDayIndex, error) {
	if v > uint8(LoginBonusDayIndexDay14) {
		return LoginBonusDayIndexDay1, ErrInvalidLoginBonusDayIndex
	}
	return LoginBonusDayIndex(v), nil
}
