package value_offer

import "errors"

type OfferFuncType uint8

const (
	// 総クリエ量
	OfferFuncTypeCreaAmount OfferFuncType = iota + 1
	// 編成してクエストクリアした回数
	OfferFuncTypeQuestClearCount
	// とっておきレベル
	OfferFuncTypeSpecialLevelCount
)

var ErrInvalidOfferFuncType = errors.New("invalid OfferFuncType")

func NewOfferFuncType(v uint8) (OfferFuncType, error) {
	if v < uint8(OfferFuncTypeCreaAmount) || v > uint8(OfferFuncTypeSpecialLevelCount) {
		return OfferFuncTypeCreaAmount, ErrInvalidOfferFuncType
	}
	return OfferFuncType(v), nil
}
