package value_present

import "errors"

type PresentType uint8

const (
	PresentTypeNone PresentType = iota
	PresentTypeCharacter
	PresentTypeItem
	PresentTypeWeapon
	PresentTypeTownFacility
	PresentTypeRoomObject
	PresentTypeMasterOrb
	PresentTypeKiraraPoint
	PresentTypeGold
	PresentTypeUnlimitedGem
	PresentTypeLimitedGem
	PresentTypePackageItem
	PresentTypeAllowRoomObject
	PresentTypeMasterOrbLvUp
	PresentTypeAchievement
)

var ErrPresentTypeInvalid = errors.New("invalid present type")

func NewPresentType(value uint8) (PresentType, error) {
	if value > 14 {
		return PresentTypeNone, ErrPresentTypeInvalid
	}
	return PresentType(value), nil
}
