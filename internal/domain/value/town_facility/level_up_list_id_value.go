package value_town_facility

import "errors"

type LevelUpListId int8

const (
	LevelUpListIdNone LevelUpListId = iota
	LevelUpListIdTitle
	LevelUpListIdWeaponMaterial
	LevelUpListIdGold
	LevelUpListIdClass
)

var ErrInvalidLevelUpListId = errors.New("invalid LevelUpListId")

func NewLevelUpListId(v uint8) (LevelUpListId, error) {
	if v > uint8(LevelUpListIdClass) {
		return LevelUpListIdNone, ErrInvalidLevelUpListId
	}
	return LevelUpListId(v), nil
}
