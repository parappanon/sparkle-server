package middleware

import (
	"bytes"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
)

/** Mock handler that return success response if session middleware works well */
func getDecryptionMockHandler() http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		body, err := io.ReadAll(r.Body)
		if err != nil {
			panic(err)
		}
		w.WriteHeader(http.StatusOK)
		w.Write(body)
	}
	return http.HandlerFunc(fn)
}

func TestDecryptionMiddleware(t *testing.T) {
	// Initialize required instances to run
	key := encrypt.LoadKey()
	padding := encrypt.LoadPad()
	ignoredRoutes := GetAuthIgnoredRoutes()
	// Test cases
	tests := []struct {
		name string
		k    func(next http.HandlerFunc) http.HandlerFunc
		path string
		body string
		want string
	}{
		{
			name: "get with valid session return ok",
			k:    NewAppDecryptionMiddleware(&ignoredRoutes, &key, &padding),
			path: "/api/player/push_token/set",
			body: "xyllN14JTu7ZWk0G3KkpaGtnH19mJgtCWg+YyKnCq8o6DaoDX5CV+I/8z4s9PVcvFS3j8XZUIXT8+M6rzTdPA169RXpYxscDcihiAzvExs9L48s5bZ+HKlQx/Sh9EgGq",
			want: "{\"pushToken\":\"9496F60CBBC0329ED001053CA16E57DA207B3810B2206F974B652FB663D0AE10\"}",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Create test server
			ts := httptest.NewServer(tt.k(getDecryptionMockHandler()))
			t.Cleanup(func() {
				ts.Close()
			})
			// Create a dummy request
			req, err := http.NewRequest(
				"POST",
				ts.URL+tt.path,
				bytes.NewBuffer([]byte(tt.body)),
			)
			if err != nil {
				t.Error(err)
			}
			req.Header.Set("Content-Type", "application/json; charset=UTF-8")
			// Send a request to test server
			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				t.Error(err)
			}
			t.Cleanup(func() {
				err := resp.Body.Close()
				if err != nil {
					t.Error(err)
				}
			})
			// Read response
			buf, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Error(err)
			}
			if want := tt.want; string(buf) != want {
				t.Errorf("\ngot %v\nwant %v\n", string(buf), want)
			}
		})
	}
}
