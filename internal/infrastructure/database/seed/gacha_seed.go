package seed

import (
	"encoding/json"

	model_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/model/gacha"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedGachas(db *gorm.DB) {
	gachasFile, err := Read("gachas")
	if err != nil {
		return
	}
	var gachas []model_gacha.Gacha
	err = json.Unmarshal(gachasFile, &gachas)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(gachas, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
