package seed

import (
	"encoding/json"

	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedEvoTableLimitBreak(db *gorm.DB) {
	tableFile, err := Read("evo_table_limit_break")
	if err != nil {
		return
	}
	var evoTableLimitBreak []model_evo_table.EvoTableLimitBreak
	err = json.Unmarshal(tableFile, &evoTableLimitBreak)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(evoTableLimitBreak, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
