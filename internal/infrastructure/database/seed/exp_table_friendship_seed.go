package seed

import (
	"encoding/json"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedExpTableFriendship(db *gorm.DB) {
	tableFile, err := Read("exp_table_friendships")
	if err != nil {
		return
	}
	var expTableFriendships []model_exp_table.ExpTableFriendship
	err = json.Unmarshal(tableFile, &expTableFriendships)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(expTableFriendships, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
