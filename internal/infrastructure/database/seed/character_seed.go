package seed

import (
	"encoding/json"

	model_character "gitlab.com/kirafan/sparkle/server/internal/domain/model/character"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedCharacters(db *gorm.DB) {
	charactersFile, err := Read("characters")
	if err != nil {
		return
	}
	var characters []model_character.Character
	err = json.Unmarshal(charactersFile, &characters)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(characters, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
