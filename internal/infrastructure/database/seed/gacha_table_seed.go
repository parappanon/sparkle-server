package seed

import (
	"encoding/json"

	model_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/model/gacha"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedGachaTables(db *gorm.DB) {
	gachaTablesFile, err := Read("gacha_tables")
	if err != nil {
		return
	}
	var gachaTables []model_gacha.GachaTable
	err = json.Unmarshal(gachaTablesFile, &gachaTables)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(gachaTables, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
