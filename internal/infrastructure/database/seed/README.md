## database/seed

This folder contains database seed files.
Seed means initial data when server first run.

- For example
  - Resource hashes
  - Items
  - Test data for development

### NOTE

- Since seed files has chance to get copyright problem, they are not in git repository.
