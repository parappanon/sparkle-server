package seed

import (
	"encoding/json"

	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedPresents(db *gorm.DB) {
	presentsFile, err := Read("presents")
	if err != nil {
		return
	}
	var presents []model_present.Present
	err = json.Unmarshal(presentsFile, &presents)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(presents, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
