package migrate

import (
	model_chapter "gitlab.com/kirafan/sparkle/server/internal/domain/model/chapter"
	model_character "gitlab.com/kirafan/sparkle/server/internal/domain/model/character"
	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	model_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/model/gacha"
	model_item "gitlab.com/kirafan/sparkle/server/internal/domain/model/item"
	model_item_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/item_table"
	model_level_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/level_table"
	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	model_named_type "gitlab.com/kirafan/sparkle/server/internal/domain/model/named_type"
	model_offer "gitlab.com/kirafan/sparkle/server/internal/domain/model/offer"
	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	model_room_object "gitlab.com/kirafan/sparkle/server/internal/domain/model/room_object"
	model_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/model/town_facility"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	model_version "gitlab.com/kirafan/sparkle/server/internal/domain/model/version"
	"gorm.io/gorm"
)

func AutoMigrate(db *gorm.DB) {
	db.Exec("PRAGMA foreign_keys = ON", nil)
	db.AutoMigrate(
		// Version models
		&model_version.Version{},
		// User models
		&model_user.User{},
		&model_user.ClearedAdvId{},
		&model_user.FavoriteMember{},
		&model_user.ItemSummary{},
		&model_user.ManagedAbilityBoard{},
		&model_user.ManagedAbilityBoardEquipItemId{},
		&model_user.ManagedBattleParty{},
		&model_user.ManagedCharacter{},
		&model_user.ManagedTown{},
		&model_user.ManagedTownFacility{},
		&model_user.ManagedFieldPartyMember{},
		&model_user.ManagedNamedType{},
		&model_user.ManagedRoom{},
		&model_user.ManagedRoomArrangeData{},
		&model_user.ManagedRoomObject{},
		&model_user.ManagedMasterOrb{},
		&model_user.ManagedWeapon{},
		&model_user.OfferTitleType{},
		&model_user.ShownTipId{},
		&model_user.SupportCharacterManagedCharacterId{},
		&model_user.SupportCharacterManagedWeaponId{},
		&model_user.SupportCharacter{},
		&model_user.QuestLog{},
		&model_user.QuestAccess{},
		&model_user.UserMission{},
		&model_user.UserPresent{},
		&model_user.UserGacha{},
		&model_user.UserLoginBonus{},
		&model_user.UserAchievement{},
		// Quest models
		&model_quest.Quest{},
		&model_quest.QuestNpc{},
		&model_quest.QuestFirstClearReward{},
		&model_quest.QuestStaminaReduction{},
		&model_quest.OfferQuest{},
		&model_quest.CharacterQuest{},
		&model_quest.EventQuest{},
		&model_quest.EventQuestCondId{},
		&model_quest.EventQuestPeriod{},
		&model_quest.EventQuestPeriodGroup{},
		&model_quest.EventQuestPeriodGroupQuest{},
		&model_quest.EventQuestPeriodGroupQuestCondEventQuestId{},
		&model_quest.QuestWave{},
		&model_quest.QuestWaveRandom{},
		&model_quest.QuestWaveDrop{},
		// Chapter models
		&model_chapter.Chapter{},
		&model_chapter.ChapterQuestId{},
		// Mission models
		&model_mission.Mission{},
		&model_mission.MissionReward{},
		// Item models
		&model_item.Item{},
		// RoomObject models
		&model_room_object.RoomObject{},
		// TownFacility models
		&model_town_facility.TownFacility{},
		&model_level_table.LevelTableTownFacility{},
		&model_level_table.LevelTableTownFacilityFieldItemDropId{},
		&model_level_table.LevelTableTownFacilityFieldItemDropProbability{},
		&model_item_table.ItemTableTownFacility{},
		// Character models
		&model_character.Character{},
		&model_named_type.NamedType{},
		// Present models
		&model_present.Present{},
		// LoginBonus models
		&model_login_bonus.LoginBonusDayBonusItem{},
		&model_login_bonus.LoginBonusDay{},
		&model_login_bonus.LoginBonus{},
		// Offer models
		&model_offer.Offer{},
		&model_offer.OfferReward{},
		// Gacha models
		&model_gacha.Gacha{},
		&model_gacha.GachaSelectionCharacter{},
		&model_gacha.GachaDrawPointTradable{},
		&model_gacha.GachaStep{},
		&model_gacha.GachaStepBonusItem{},
		&model_gacha.GachaDailyFree{},
		&model_gacha.GachaTable{},
		// ExpTable models
		&model_exp_table.ExpTableRank{},
		&model_exp_table.ExpTableCharacter{},
		&model_exp_table.ExpTableWeapon{},
		&model_exp_table.ExpTableFriendship{},
		&model_exp_table.ExpTableSkill{},
		// EvoTable models
		&model_evo_table.EvoTableLimitBreak{},
		&model_evo_table.EvoTableEvolution{},
		&model_evo_table.EvoTableEvolutionRequiredItem{},
	)
}
