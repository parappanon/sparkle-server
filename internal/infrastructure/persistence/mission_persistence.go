package persistence

import (
	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type missionRepositoryImpl struct {
	Conn *gorm.DB
}

func NewMissionRepositoryImpl(conn *gorm.DB) repository.MissionRepository {
	return &missionRepositoryImpl{Conn: conn}
}

func (qr *missionRepositoryImpl) FindMissions(query *model_mission.Mission, criteria map[string]interface{}, associations []string) ([]*model_mission.Mission, error) {
	var missions []*model_mission.Mission
	var result *gorm.DB
	chain := qr.Conn
	for _, association := range associations {
		chain = chain.Preload(association)
	}
	if query != nil {
		result = chain.Where(query).Find(&missions)
	} else {
		result = chain.Where(criteria).Find(&missions)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return missions, nil
}

func (qr *missionRepositoryImpl) FindMission(query *model_mission.Mission, criteria map[string]interface{}, associations []string) (*model_mission.Mission, error) {
	var mission *model_mission.Mission
	var result *gorm.DB
	if query != nil {
		result = qr.Conn.Where(&query).First(&mission)
	} else {
		result = qr.Conn.Where(criteria).First(&mission)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return mission, nil
}
