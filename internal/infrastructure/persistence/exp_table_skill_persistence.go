package persistence

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type expTableSkillRepositoryImpl struct {
	Conn *gorm.DB
}

func NewExpTableSkillRepositoryImpl(conn *gorm.DB) repository.ExpTableSkillRepository {
	return &expTableSkillRepositoryImpl{Conn: conn}
}

func (rp *expTableSkillRepositoryImpl) FindExpTableSkills(query *model_exp_table.ExpTableSkill, criteria map[string]interface{}) ([]*model_exp_table.ExpTableSkill, error) {
	var datas []*model_exp_table.ExpTableSkill
	var result *gorm.DB
	chain := rp.Conn
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *expTableSkillRepositoryImpl) FindExpTableSkill(query *model_exp_table.ExpTableSkill, criteria map[string]interface{}) (*model_exp_table.ExpTableSkill, error) {
	var data *model_exp_table.ExpTableSkill
	var result *gorm.DB
	chain := rp.Conn
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
