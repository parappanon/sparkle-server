package persistence

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type questRepositoryImpl struct {
	Conn *gorm.DB
}

// NewQuestRepositoryImpl quest repositoryのコンストラクタ
func NewQuestRepositoryImpl(conn *gorm.DB) repository.QuestRepository {
	return &questRepositoryImpl{Conn: conn}
}

func (qr *questRepositoryImpl) FindQuests(query *model_quest.Quest, criteria map[string]interface{}, associations []string) ([]*model_quest.Quest, error) {
	var quests []*model_quest.Quest
	var result *gorm.DB
	chain := qr.Conn
	for _, association := range associations {
		chain = chain.Preload(association)
	}
	if query != nil {
		result = chain.Where(query).Find(&quests)
	} else {
		result = chain.Where(criteria).Find(&quests)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return quests, nil
}

func (qr *questRepositoryImpl) FindQuest(query *model_quest.Quest, criteria map[string]interface{}, associations []string) (*model_quest.Quest, error) {
	var quest *model_quest.Quest
	var result *gorm.DB
	chain := qr.Conn
	for _, association := range associations {
		chain = chain.Preload(association)
	}
	if query != nil {
		result = chain.Where(&query).First(&quest)
	} else {
		result = chain.Where(criteria).First(&quest)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return quest, nil
}
