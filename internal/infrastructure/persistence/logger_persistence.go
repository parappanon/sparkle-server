package persistence

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type LogLevel = int

const (
	DebugLevel LogLevel = iota
	InfoLevel
	WarnLevel
	ErrorLevel
)

type loggerRepository struct {
	logger   *logrus.Logger
	logLevel LogLevel
}

// NewTaskRepository task repositoryのコンストラクタ
func NewLoggerRepository(logger *logrus.Logger, logLevel LogLevel) repository.LoggerRepository {
	switch logLevel {
	case DebugLevel:
		logger.SetLevel(logrus.DebugLevel)
	case InfoLevel:
		logger.SetLevel(logrus.InfoLevel)
	case WarnLevel:
		logger.SetLevel(logrus.WarnLevel)
	case ErrorLevel:
		logger.SetLevel(logrus.ErrorLevel)
	}
	return &loggerRepository{logLevel: logLevel, logger: logger}
}

func (l *loggerRepository) GetLogger() *logrus.Logger {
	return l.logger
}

func (l *loggerRepository) Debug(msg string) {
	l.logger.Debug(msg)
}

func (l *loggerRepository) Info(msg string) {
	l.logger.Info(msg)
}

func (l *loggerRepository) Warn(msg string) {
	l.logger.Warn(msg)
}

func (l *loggerRepository) Error(msg string) {
	l.logger.Error(msg)
}

func (l *loggerRepository) Fatal(msg string) {
	l.logger.Fatal(msg)
}
