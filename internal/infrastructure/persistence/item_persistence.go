package persistence

import (
	model_item "gitlab.com/kirafan/sparkle/server/internal/domain/model/item"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"

	"gorm.io/gorm"
)

type itemRepositoryImpl struct {
	Conn *gorm.DB
}

func NewItemRepositoryImpl(conn *gorm.DB) repository.ItemRepository {
	return &itemRepositoryImpl{Conn: conn}
}

func (rp *itemRepositoryImpl) FindItems(query *model_item.Item, criteria map[string]interface{}, associations *[]string) ([]*model_item.Item, error) {
	var datas []*model_item.Item
	var result *gorm.DB
	chain := rp.Conn
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *itemRepositoryImpl) FindItem(query *model_item.Item, criteria map[string]interface{}, associations *[]string) (*model_item.Item, error) {
	var data *model_item.Item
	var result *gorm.DB
	chain := rp.Conn
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		result = chain.Where(criteria).First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

func (rp *itemRepositoryImpl) GetWeaponUpgradeAmount(itemId int64) (int32, error) {
	var data *model_item.Item
	result := rp.Conn.Model(&model_item.Item{}).Where(
		"item_id", itemId,
	).Where(
		"category IN (?)", []value_item.ItemCategory{
			value_item.ItemCategoryWeaponMaterialSymbol,
			value_item.ItemCategoryWeaponMaterialCrest,
			value_item.ItemCategoryWeaponMaterialChapter1,
			value_item.ItemCategoryWeaponMaterialChapter2,
			value_item.ItemCategoryWeaponMaterialChapter3,
			value_item.ItemCategoryWeaponMaterialChapter4,
			value_item.ItemCategoryWeaponMaterialChapter5,
			value_item.ItemCategoryWeaponMaterialChapter6,
			value_item.ItemCategoryWeaponMaterialChapter7,
			value_item.ItemCategoryWeaponMaterialChapter8,
		},
	).Select("type_arg1").First(&data)
	if result.Error != nil {
		return 0, result.Error
	}
	// NOTE: Item database doesn't include class type info but the "ExpBonusItem" is defined in weapon database
	return data.TypeArg1, nil
}

func (rp *itemRepositoryImpl) GetCharacterUpgradeAmount(itemId int64) (int32, int32, error) {
	var data *model_item.Item
	result := rp.Conn.Model(&model_item.Item{}).Where(
		"item_id", itemId,
	).Where(
		"category IN (?)", []value_item.ItemCategory{
			value_item.ItemCategorySeedSmall,
			value_item.ItemCategorySeedNormal,
			value_item.ItemCategorySeedLarge,
			value_item.ItemCategorySeedExtraLarge,
		},
	).Select("type_arg1, type_arg2").First(&data)
	if result.Error != nil {
		return 0, 0, result.Error
	}
	return data.TypeArg1, data.TypeArg2, nil
}
