package persistence

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type expTableRankRepositoryImpl struct {
	Conn *gorm.DB
}

func NewExpTableRankRepositoryImpl(conn *gorm.DB) repository.ExpTableRankRepository {
	return &expTableRankRepositoryImpl{Conn: conn}
}

func (rp *expTableRankRepositoryImpl) FindExpTableRanks(query *model_exp_table.ExpTableRank, criteria map[string]interface{}) ([]*model_exp_table.ExpTableRank, error) {
	var datas []*model_exp_table.ExpTableRank
	var result *gorm.DB
	chain := rp.Conn
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *expTableRankRepositoryImpl) FindExpTableRank(query *model_exp_table.ExpTableRank, criteria map[string]interface{}) (*model_exp_table.ExpTableRank, error) {
	var data *model_exp_table.ExpTableRank
	var result *gorm.DB
	chain := rp.Conn
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
