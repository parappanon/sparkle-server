package persistence

import (
	model_version "gitlab.com/kirafan/sparkle/server/internal/domain/model/version"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"

	"gorm.io/gorm"
)

type versionRepositoryImpl struct {
	Conn *gorm.DB
}

// NewVersionRepositoryImpl version repositoryのコンストラクタ
func NewVersionRepositoryImpl(conn *gorm.DB) repository.VersionRepository {
	return &versionRepositoryImpl{Conn: conn}
}

// Create versionの保存
func (ur *versionRepositoryImpl) Create(version *model_version.Version) (*model_version.Version, error) {
	if err := ur.Conn.Create(&version).Error; err != nil {
		return nil, err
	}

	return version, nil
}

func (ur *versionRepositoryImpl) Update(version *model_version.Version) (*model_version.Version, error) {
	if err := ur.Conn.Model(&version).Updates(&version).Error; err != nil {
		return nil, err
	}

	return version, nil
}

func (ur *versionRepositoryImpl) FindByPlatformAndVersion(platform value_version.Platform, version value_version.Version) (*model_version.Version, error) {
	v := &model_version.Version{Platform: platform, Version: version}
	if err := ur.Conn.Where(&v).First(&v).Error; err != nil {
		return nil, err
	}
	return v, nil
}
