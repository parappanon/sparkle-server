package persistence

import (
	model_chapter "gitlab.com/kirafan/sparkle/server/internal/domain/model/chapter"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type chapterRepositoryImpl struct {
	Conn *gorm.DB
}

func NewChapterRepositoryImpl(conn *gorm.DB) repository.ChapterRepository {
	return &chapterRepositoryImpl{Conn: conn}
}

func (qr *chapterRepositoryImpl) GetAll() ([]*model_chapter.Chapter, error) {
	var chapters []*model_chapter.Chapter
	result := qr.Conn.Preload("QuestIds").Find(&chapters)
	if result.Error != nil {
		return nil, result.Error
	}
	return chapters, nil
}
