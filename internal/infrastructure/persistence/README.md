## infrastructure/persistence

- This folder contains persistence layer.
- For example
  - Save the data to database
  - Find the data from database

main <-> middleware <-> router(interfaces) <-> usecase <-> repository <-> model(gorm) <-> **db(persistence)**
