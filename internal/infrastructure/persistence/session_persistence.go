package persistence

import (
	"fmt"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type sessionRepositoryImpl struct {
	Conn *gorm.DB
}

// NewSessionRepositoryImpl
func NewSessionRepositoryImpl(conn *gorm.DB) repository.SessionRepository {
	return &sessionRepositoryImpl{Conn: conn}
}

// Find the user session and check the request is valid
func (sr *sessionRepositoryImpl) Validate(session string) (int32, error) {
	if session == "" {
		return -1, fmt.Errorf("session is empty")
	}
	user := &model_user.User{}
	if err := sr.Conn.Where(&model_user.User{Session: session}).First(&user).Error; err != nil {
		return -1, err
	}
	return int32(user.Id), nil
}
