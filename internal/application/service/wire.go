//go:build wireinject
// +build wireinject

package service

import (
	"github.com/google/wire"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/wires_providers"
	"gorm.io/gorm"
)

func InitializeUserUsecase(logger repository.LoggerRepository, db *gorm.DB) usecase.UserUsecase {
	wire.Build(
		wires_providers.UserUsecaseSet,
	)
	return nil
}

func InitializeUserPresentService(logger repository.LoggerRepository, db *gorm.DB) UserPresentService {
	wire.Build(
		wires_providers.UserUsecaseSet,
		wires_providers.CharacterUsecaseSet,
		wires_providers.NamedTypeUsecaseSet,
		NewUserPresentService,
	)
	return nil
}

func InitializeUserGachaService(logger repository.LoggerRepository, db *gorm.DB) PlayerGachaService {
	wire.Build(
		wires_providers.UserUsecaseSet,
		wires_providers.GachaUsecaseSet,
		wires_providers.NamedTypeUsecaseSet,
		wires_providers.CharacterUsecaseSet,
		wires_providers.ItemUsecaseSet,
		NewPlayerGachaService,
	)
	return nil
}

func InitializeUserQuestService(logger repository.LoggerRepository, db *gorm.DB) UserQuestService {
	wire.Build(
		wires_providers.UserUsecaseSet,
		wires_providers.CharacterUsecaseSet,
		wires_providers.QuestUsecaseSet,
		wires_providers.QuestWaveUsecaseSet,
		wires_providers.ExpTableCharacterUsecaseSet,
		wires_providers.ExpTableSkillUsecaseSet,
		wires_providers.ExpTableRankUsecaseSet,
		wires_providers.ExpTableFriendshipUsecaseSet,
		NewUserQuestService,
	)
	return nil
}

func InitializeUserTownFacilityService(logger repository.LoggerRepository, db *gorm.DB) UserTownFacilityService {
	wire.Build(
		wires_providers.UserUsecaseSet,
		wires_providers.TownFacilityUsecaseSet,
		wires_providers.LevelTableTownFacilityUsecaseSet,
		wires_providers.ItemTableTownFacilityUsecaseSet,
		NewUserTownFacilityService,
	)
	return nil
}
