package service

import (
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type PlayerFieldPartyService interface {
	GetAllPlayerFieldPartyMember(internalUserId uint) ([]model_user.ManagedFieldPartyMember, error)
	RemoveAllPlayerFieldPartyMember(internalUserId uint, managedPartyMemberIds []int64) error
}

func NewPlayerFieldPartyService(
	uu usecase.UserUsecase,
) PlayerFieldPartyService {
	return &playerFieldPartyService{uu}
}

type playerFieldPartyService struct {
	uu usecase.UserUsecase
}

func (s *playerFieldPartyService) GetAllPlayerFieldPartyMember(internalUserId uint) ([]model_user.ManagedFieldPartyMember, error) {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{ManagedFieldPartyMembers: true})
	if err != nil {
		return nil, err
	}
	return user.ManagedFieldPartyMembers, err
}

func (s *playerFieldPartyService) RemoveAllPlayerFieldPartyMember(internalUserId uint, managedPartyMemberIds []int64) error {
	// STUB
	return errors.New("this endpoint is not yet implemented")
}
