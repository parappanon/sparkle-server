package service

import (
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type PlayerBadgeService interface {
	GetPlayerBadge(internalUserId uint) (uint8, uint8, uint8, error)
}

type playerBadgeService struct {
	uu usecase.UserUsecase
}

func NewPlayerBadgeService(uu usecase.UserUsecase) PlayerBadgeService {
	return &playerBadgeService{uu}
}

func (s *playerBadgeService) GetPlayerBadge(internalUserId uint) (uint8, uint8, uint8, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		return 0, 0, 0, err
	}
	// Get player badge
	friendProposedCount := user.FriendProposedCount
	presentCount := user.PresentCount
	trainingCount := user.TrainingCount
	return friendProposedCount, presentCount, trainingCount, nil
}
