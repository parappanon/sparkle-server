package service

import (
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type ScheduleService interface {
	GetSchedules(internalId uint, managedFieldPartyMemberIds []uint) ([]*model_user.ManagedFieldPartyMember, error)
}

type scheduleService struct {
	uu usecase.UserUsecase
	su usecase.ScheduleUsecase
}

func NewScheduleService(
	uu usecase.UserUsecase,
	su usecase.ScheduleUsecase,
) ScheduleService {
	return &scheduleService{uu, su}
}

var ErrScheduleServiceUserNotFound = errors.New("user not found")
var ErrScheduleServiceManagedCharacterNotFound = errors.New("specified managed character not found")

func (s *scheduleService) GetSchedules(internalId uint, managedFieldPartyMemberIds []uint) ([]*model_user.ManagedFieldPartyMember, error) {
	user, err := s.uu.GetUserByInternalId(internalId, repository.UserRepositoryParam{ManagedFieldPartyMembers: true})
	if err != nil {
		return nil, ErrScheduleServiceUserNotFound
	}

	// Get managed characters specified at req
	targetManagedFieldPartyMembers := []*model_user.ManagedFieldPartyMember{}
	for _, managedCharacterId := range managedFieldPartyMemberIds {
		var managedCharacter *model_user.ManagedFieldPartyMember = nil
		for _, mc := range user.ManagedFieldPartyMembers {
			// FIXME: Remove this force type assertion
			if mc.ManagedCharacterId == value_user.ManagedCharacterId(managedCharacterId) {
				managedCharacter = &mc
				break
			}
		}
		if managedCharacter == nil {
			return nil, ErrScheduleServiceManagedCharacterNotFound
		}
		targetManagedFieldPartyMembers = append(targetManagedFieldPartyMembers, managedCharacter)
	}

	// Create request param target character ids
	var targetCharacterIds []value_character.CharacterId
	for _, managedCharacter := range targetManagedFieldPartyMembers {
		characterId, err := value_character.NewCharacterId(uint32(managedCharacter.CharacterId))
		if err != nil {
			return nil, err
		}
		targetCharacterIds = append(targetCharacterIds, characterId)
	}

	// Create request param target schedules
	var targetSchedules []string
	for _, managedCharacter := range targetManagedFieldPartyMembers {
		targetSchedules = append(targetSchedules, *managedCharacter.ScheduleTable)
	}

	// Refresh user schedule
	schedules, err := s.su.RefreshSchedule(targetSchedules)
	if err != nil {
		return nil, err
	}

	// Update managed field party member schedules
	var resp []*model_user.ManagedFieldPartyMember
	for _, managedCharacterId := range managedFieldPartyMemberIds {
		for i, mc := range user.ManagedFieldPartyMembers {
			// FIXME: Remove this force type assertion
			if mc.ManagedCharacterId == value_user.ManagedCharacterId(managedCharacterId) {
				var targetCharacterIndex int
				for j, c := range targetCharacterIds {
					if int(c) == int(mc.CharacterId) {
						targetCharacterIndex = j
						break
					}
				}
				user.ManagedFieldPartyMembers[i].ScheduleTable = &schedules[targetCharacterIndex]
				resp = append(resp, &user.ManagedFieldPartyMembers[i])
			}
		}
	}

	// Update user
	_, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ManagedFieldPartyMembers: true})
	if err != nil {
		return nil, err
	}

	return resp, nil
}
