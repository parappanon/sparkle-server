package service

import (
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_general_flag_save "gitlab.com/kirafan/sparkle/server/internal/domain/value/general_flag_save"
)

type PlayerGeneralFlagSaveService interface {
	GeneralFlagSavePlayer(
		internalUserId uint,
		flagType value_general_flag_save.GeneralFlagSaveType,
		flagValue value_general_flag_save.GeneralFlagSaveData,
	) error
}

func NewPlayerGeneralFlagSaveService(
	uu usecase.UserUsecase,
) PlayerGeneralFlagSaveService {
	return &playerGeneralFlagSaveService{uu}
}

type playerGeneralFlagSaveService struct {
	uu usecase.UserUsecase
}

func (s *playerGeneralFlagSaveService) GeneralFlagSavePlayer(
	internalUserId uint,
	flagType value_general_flag_save.GeneralFlagSaveType,
	flagValue value_general_flag_save.GeneralFlagSaveData,
) error {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		return errors.New("user not found")
	}
	switch flagType {
	case value_general_flag_save.GeneralFlagSaveTypePart1:
		user.LastOpenedPart1ChapterId = int8(flagValue)
		// Chapter2 save new chapter call
	case value_general_flag_save.GeneralFlagSaveTypePart2:
		user.LastOpenedPart2ChapterId = int8(flagValue)
	}
	if _, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{}); err != nil {
		return errors.New("db error")
	}
	return nil
}
