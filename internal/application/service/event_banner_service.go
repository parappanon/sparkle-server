package service

import (
	model_event_banner "gitlab.com/kirafan/sparkle/server/internal/domain/model/event_banner"
)

type EventBannerService interface {
	GetAllEventBanner() ([]model_event_banner.EventBanner, error)
}

type eventBannerService struct {
}

func NewEventBannerService() EventBannerService {
	return &eventBannerService{}
}

func (s *eventBannerService) GetAllEventBanner() ([]model_event_banner.EventBanner, error) {
	resp := []model_event_banner.EventBanner{
		{
			// maybe database primary key
			Id:        1139,
			StartAt:   "2022-11-07T17:00:00",
			DispEndAt: "2099-02-28T15:59:59",
			// maybe deprecated
			EndAt: "2099-02-28T15:59:59",
			// no idea...
			Category: 1,
			// maybe same as gacha/get_all's id
			GachaId: -1,
			// maybe same as https://asset.kirafan.moe/#/texture/eventbanner.muast
			ImgId: "EventBanner_E00100",
			// maybe deprecated
			OffsetX: 0.0,
			// maybe deprecated
			OffsetY: 0.0,
			// maybe deprecated
			PickupCharacterIds: nil,
		},
		{
			Id:                 1140,
			StartAt:            "2022-11-07T17:00:00",
			DispEndAt:          "2099-02-28T15:59:59",
			EndAt:              "2099-02-28T15:59:59",
			Category:           1,
			GachaId:            -1,
			ImgId:              "EventBanner_E00101",
			OffsetX:            0.0,
			OffsetY:            0.0,
			PickupCharacterIds: nil,
		},
	}
	return resp, nil
}
