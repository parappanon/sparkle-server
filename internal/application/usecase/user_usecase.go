package usecase

import (
	"errors"
	"time"

	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/value/login_bonus"
	value_mission "gitlab.com/kirafan/sparkle/server/internal/domain/value/mission"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type UserUsecase interface {
	CreateUser(uuid string, username string, presents []*model_present.Present, missions []*model_mission.Mission, loginBonuses []*model_login_bonus.LoginBonus) (*model_user.User, error)
	UpdateUser(user *model_user.User, param repository.UserRepositoryParam) (*model_user.User, error)
	SetupUser(userId uint) (*model_user.User, error)
	GetUserByInternalId(internalId uint, param repository.UserRepositoryParam) (*model_user.User, error)
	GetUserBySession(deviceUUId string, sessionUUId string) (*model_user.User, error)
	GetUserByFriendCode(code string) (*model_user.User, error)
	GetUserByMoveCode(code string) (*model_user.User, error)
	GetLatestUserQuestLog(internalId uint) (*model_user.QuestLog, error)
	InsertUserQuestLog(internalId uint, questLog model_user.QuestLog) error
	UpdateUserQuestLog(questLog model_user.QuestLog) error
	GetUserClearRanks(internalId uint) ([]*model_user.QuestClearRank, error)
}

var ErrUserSetupAlreadyDone = errors.New("the user setup is already done")
var ErrUserSetupNotEnoughCharacter = errors.New("the user does not have enough character")
var ErrUserSetupNotDesiredStep = errors.New("the user stepCode is not desired step")

type userUsecase struct {
	userRepo repository.UserRepository
	logger   repository.LoggerRepository
}

func NewUserUsecase(userRepo repository.UserRepository, logger repository.LoggerRepository) UserUsecase {
	return &userUsecase{userRepo, logger}
}

func (uu *userUsecase) CreateUser(uuid string, name string, presents []*model_present.Present, missions []*model_mission.Mission, loginBonuses []*model_login_bonus.LoginBonus) (*model_user.User, error) {
	if u, err := uu.userRepo.FindUserByUUID(uuid, repository.UserRepositoryParam{}); u != nil {
		return nil, err
	}
	user := model_user.NewUser(uuid, name)
	// Inject initial missions
	var userMissions []model_user.UserMission
	for _, mission := range missions {
		userMissions = append(userMissions,
			model_user.UserMission{
				UserId:    user.Id,
				LimitTime: time.Now().AddDate(0, 0, int(value_mission.EXPIRE_TUTORIAL_MISSION)),
				Mission:   *mission,
				Rate:      0,
				State:     value_mission.MissionStateNotCleared,
			},
		)
	}
	user.Missions = userMissions
	// Inject initial presents
	var userPresents []model_user.UserPresent
	for _, present := range presents {
		userPresents = append(userPresents,
			model_user.UserPresent{
				UserId:     user.Id,
				Present:    *present,
				ReceivedAt: nil,
				DeadlineAt: time.Now().AddDate(0, 0, int(value_mission.EXPIRE_TUTORIAL_PRESENT)),
				Type:       present.Type,
				Amount:     present.Amount,
				ObjectId:   present.ObjectId,
			},
		)
	}
	user.Presents = userPresents
	// Inject initial gacha
	user.Gachas = []model_user.UserGacha{
		model_user.NewUserGacha(user.Id, 1),
	}
	// Inject initial login bonuses
	var userLoginBonuses []model_user.UserLoginBonus
	for _, loginBonus := range loginBonuses {
		userLoginBonuses = append(userLoginBonuses,
			model_user.UserLoginBonus{
				CreatedAt:          time.Now(),
				UpdatedAt:          time.Now().AddDate(0, 0, -1),
				DeletedAt:          nil,
				LoginBonusDayIndex: value_login_bonus.LoginBonusDayIndexDay1,
				LoginBonusId:       loginBonus.BonusId,
				UserId:             user.Id,
			},
		)
	}
	user.LoginBonuses = userLoginBonuses

	createdUser, err := uu.userRepo.CreateUser(&user)
	if err != nil {
		return nil, err
	}

	newManagedRooms := model_user.NewInitialManagedRooms(createdUser.ManagedRoomObjects)
	user.ManagedRooms = newManagedRooms

	initializedUser, err := uu.userRepo.UpdateUser(&user, repository.UserRepositoryParam{ManagedRooms: true})
	if err != nil {
		return nil, err
	}
	return initializedUser, nil
}

func (uu *userUsecase) SetupUser(userId uint) (*model_user.User, error) {
	u, err := uu.userRepo.FindUserById(userId, repository.UserRepositoryParam{Entire: true})
	if err != nil {
		return nil, err
	}
	if u.StepCode != value_user.StepCodeAdvCreaDone {
		return u, ErrUserSetupNotDesiredStep
	}
	if len(u.ManagedCharacters) < 5 {
		return u, ErrUserSetupNotEnoughCharacter
	}
	if u.ManagedBattleParties[0].ManagedCharacterId1 != -1 {
		return u, ErrUserSetupAlreadyDone
	}
	if len(u.ManagedFieldPartyMembers) > 0 {
		return u, ErrUserSetupAlreadyDone
	}
	if len(u.FavoriteMembers) > 0 {
		return u, ErrUserSetupAlreadyDone
	}

	// Update battle party
	managedCharacterLen := len(u.ManagedCharacters)
	newManagedBattleParty := model_user.ManagedBattleParty{
		ManagedBattlePartyId: u.ManagedBattleParties[0].ManagedBattlePartyId,
		UserId:               u.Id,
		Name:                 "パーティー1",
		CostLimit:            0,
		ManagedCharacterId1:  int64(u.ManagedCharacters[managedCharacterLen-1].ManagedCharacterId),
		ManagedCharacterId2:  int64(u.ManagedCharacters[managedCharacterLen-2].ManagedCharacterId),
		ManagedCharacterId3:  int64(u.ManagedCharacters[managedCharacterLen-3].ManagedCharacterId),
		ManagedCharacterId4:  int64(u.ManagedCharacters[managedCharacterLen-4].ManagedCharacterId),
		ManagedCharacterId5:  int64(u.ManagedCharacters[managedCharacterLen-5].ManagedCharacterId),
		ManagedWeaponId1:     0,
		ManagedWeaponId2:     0,
		ManagedWeaponId3:     0,
		ManagedWeaponId4:     0,
		ManagedWeaponId5:     0,
		MasterOrbId:          1,
	}
	u.ManagedBattleParties[0] = newManagedBattleParty

	// Update field members
	var newManagedFieldPartyMembers []model_user.ManagedFieldPartyMember
	for i := 0; i < 5; i++ {
		newManagedFieldPartyMembers = append(newManagedFieldPartyMembers, model_user.ManagedFieldPartyMember{
			ManagedCharacterId: u.ManagedCharacters[managedCharacterLen-1-i].ManagedCharacterId,
			CharacterId:        uint64(u.ManagedCharacters[managedCharacterLen-1-i].CharacterId),
			UserId:             u.Id,
			LiveIdx:            uint8(i),
			ManagedFacilityId:  0,
			RoomId:             1,
			ScheduleTable:      nil,
			ScheduleId:         -1,
			ScheduleTag:        -1,
			TouchItemResultNo:  -1,
			Flag:               0,
			PartyDropPresents:  nil,
		})
	}
	u.ManagedFieldPartyMembers = newManagedFieldPartyMembers

	// Update favorite members
	var newManagedFavoriteMembers []model_user.FavoriteMember
	for i := 0; i < 5; i++ {
		newManagedFavoriteMembers = append(newManagedFavoriteMembers, model_user.FavoriteMember{
			ManagedCharacterId: u.ManagedCharacters[managedCharacterLen-1-i].ManagedCharacterId,
			CharacterId:        u.ManagedCharacters[managedCharacterLen-1-i].CharacterId,
			FavoriteIndex:      uint8(i),
			ArousalLevel:       u.ManagedCharacters[managedCharacterLen-1-i].ArousalLevel,
			UserId:             u.Id,
		})
	}
	u.FavoriteMembers = newManagedFavoriteMembers

	// Update user stepCode
	u.StepCode = value_user.StepCodeGachaDrawDone

	newUser, err := uu.userRepo.UpdateUser(u, repository.UserRepositoryParam{
		ManagedCharacters:        true,
		ManagedBattleParties:     true,
		ManagedFieldPartyMembers: true,
		FavoriteMembers:          true,
	})
	if err != nil {
		return nil, err
	}
	return newUser, nil
}

func (uu *userUsecase) UpdateUser(user *model_user.User, param repository.UserRepositoryParam) (*model_user.User, error) {
	newUser, err := uu.userRepo.UpdateUser(user, param)
	if err != nil {
		return nil, err
	}
	return newUser, nil
}

func (uu *userUsecase) GetUserByInternalId(internalId uint, param repository.UserRepositoryParam) (*model_user.User, error) {
	foundUser, err := uu.userRepo.FindUserById(internalId, param)
	if err != nil {
		return nil, err
	}
	return foundUser, nil
}

func (uu *userUsecase) GetUserBySession(deviceUUId string, sessionUUId string) (*model_user.User, error) {
	foundUser, err := uu.userRepo.FindUserBySession(deviceUUId, sessionUUId)
	if err != nil {
		return nil, err
	}
	return foundUser, nil
}

func (uu *userUsecase) GetUserByFriendCode(code string) (*model_user.User, error) {
	foundUser, err := uu.userRepo.FindUserByFriendCode(code)
	if err != nil {
		return nil, err
	}
	return foundUser, nil
}

func (uu *userUsecase) GetUserByMoveCode(code string) (*model_user.User, error) {
	foundUser, err := uu.userRepo.FindUserByMoveCode(code)
	if err != nil {
		return nil, err
	}
	return foundUser, nil
}

func (uu *userUsecase) GetLatestUserQuestLog(
	internalId uint,
) (*model_user.QuestLog, error) {
	order := "created_at desc"
	questLog, err := uu.userRepo.FindUserQuestLog(internalId, &model_user.QuestLog{}, &order)
	if err != nil {
		return nil, err
	}
	return questLog, nil
}

func (uu *userUsecase) GetUserClearRanks(
	internalId uint,
) ([]*model_user.QuestClearRank, error) {
	questLogs, err := uu.userRepo.GetUserQuestLogClearRanks(internalId)
	if err != nil {
		return nil, err
	}
	return questLogs, nil
}

func (uu *userUsecase) InsertUserQuestLog(
	internalId uint,
	questLog model_user.QuestLog,
) error {
	_, err := uu.userRepo.InsertUserQuestLog(internalId, &questLog)
	if err != nil {
		return err
	}
	return nil
}

func (uu *userUsecase) UpdateUserQuestLog(
	questLog model_user.QuestLog,
) error {
	_, err := uu.userRepo.UpdateUserQuestLog(&questLog)
	if err != nil {
		return err
	}
	return nil
}
