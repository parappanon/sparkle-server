package usecase

import (
	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_mission "gitlab.com/kirafan/sparkle/server/internal/domain/value/mission"
)

type MissionUsecase interface {
	GetTutorialMissions() ([]*model_mission.Mission, error)
	GetDailyMissions() ([]*model_mission.Mission, error)
	GetWeeklyMissions() ([]*model_mission.Mission, error)
	GetEventMissions() ([]*model_mission.Mission, error)
	GetNextRankUpMission(currentCount uint) (*model_mission.Mission, error)
	GetNextCharacterRelationshipMission(currentCount uint) (*model_mission.Mission, error)
	GetNextCharacterEvolutionMission(currentCount uint) (*model_mission.Mission, error)
}

type missionUsecase struct {
	mr     repository.MissionRepository
	logger repository.LoggerRepository
}

func NewMissionUsecase(mr repository.MissionRepository, logger repository.LoggerRepository) MissionUsecase {
	return &missionUsecase{mr, logger}
}

func (mu *missionUsecase) GetTutorialMissions() ([]*model_mission.Mission, error) {
	criteria := map[string]interface{}{
		"mission_insert_type": value_mission.MissionInsertTypeTutorial,
	}
	missions, err := mu.mr.FindMissions(nil, criteria, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return missions, nil
}

func (mu *missionUsecase) GetDailyMissions() ([]*model_mission.Mission, error) {
	missions, err := mu.mr.FindMissions(&model_mission.Mission{
		MissionInsertType: value_mission.MissionInsertTypeDaily,
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return missions, nil
}

func (mu *missionUsecase) GetWeeklyMissions() ([]*model_mission.Mission, error) {
	missions, err := mu.mr.FindMissions(&model_mission.Mission{
		MissionInsertType: value_mission.MissionInsertTypeWeekly,
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return missions, nil
}

func (mu *missionUsecase) GetEventMissions() ([]*model_mission.Mission, error) {
	missions, err := mu.mr.FindMissions(&model_mission.Mission{
		MissionInsertType: value_mission.MissionInsertTypeEvent,
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return missions, nil
}

func (mu *missionUsecase) GetNextRankUpMission(currentCount uint) (*model_mission.Mission, error) {
	mission, err := mu.mr.FindMission(&model_mission.Mission{
		MissionInsertType: value_mission.MissionInsertTypeRankUp,
		Rate:              uint32(currentCount),
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return mission, nil
}

func (mu *missionUsecase) GetNextCharacterRelationshipMission(currentCount uint) (*model_mission.Mission, error) {
	mission, err := mu.mr.FindMission(&model_mission.Mission{
		MissionInsertType: value_mission.MissionInsertTypeCharacterRelationship,
		Rate:              uint32(currentCount),
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return mission, nil
}

func (mu *missionUsecase) GetNextCharacterEvolutionMission(currentCount uint) (*model_mission.Mission, error) {
	mission, err := mu.mr.FindMission(&model_mission.Mission{
		MissionInsertType: value_mission.MissionInsertTypeCharacterEvolution,
		Rate:              uint32(currentCount),
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return mission, nil
}
