package usecase

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type ExpTableWeaponUsecase interface {
	GetNextExpTableWeapon(currentExp uint32, weaponType uint8) (*model_exp_table.ExpTableWeapon, error)
}

type expTableWeaponUsecase struct {
	rp     repository.ExpTableWeaponRepository
	logger repository.LoggerRepository
}

func NewExpTableWeaponUsecase(rp repository.ExpTableWeaponRepository, logger repository.LoggerRepository) ExpTableWeaponUsecase {
	return &expTableWeaponUsecase{rp, logger}
}

func (uc *expTableWeaponUsecase) GetNextExpTableWeapon(currentExp uint32, weaponType uint8) (*model_exp_table.ExpTableWeapon, error) {
	criteria := map[string]interface{}{
		"total_exp >= ?":  currentExp,
		"weapon_type = ?": weaponType,
	}
	expTableWeapon, err := uc.rp.FindExpTableWeapon(nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableWeapon, nil
}
