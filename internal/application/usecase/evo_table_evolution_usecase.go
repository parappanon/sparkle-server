package usecase

import (
	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type EvoTableEvolutionUsecase interface {
	GetEvolutionRecipe(characterId value_character.CharacterId) (*model_evo_table.EvoTableEvolution, error)
}

type evoTableEvolutionUsecase struct {
	rp     repository.EvoTableEvolutionRepository
	logger repository.LoggerRepository
}

func NewEvoTableEvolutionUsecase(rp repository.EvoTableEvolutionRepository, logger repository.LoggerRepository) EvoTableEvolutionUsecase {
	return &evoTableEvolutionUsecase{rp, logger}
}

func (u *evoTableEvolutionUsecase) GetEvolutionRecipe(characterId value_character.CharacterId) (*model_evo_table.EvoTableEvolution, error) {
	res, err := u.rp.FindByCharacterId(characterId)
	if err != nil {
		return nil, err
	}
	return res, nil
}
