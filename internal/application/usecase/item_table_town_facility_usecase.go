package usecase

import (
	model_item_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/item_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type ItemTableTownFacilityUsecase interface {
	GetItemTableTownFacility(itemNo uint32) ([]*model_item_table.ItemTableTownFacility, error)
}

type itemTableTownFacilityUsecase struct {
	rp     repository.ItemTableTownFacilityRepository
	logger repository.LoggerRepository
}

func NewItemTableTownFacilityUsecase(rp repository.ItemTableTownFacilityRepository, logger repository.LoggerRepository) ItemTableTownFacilityUsecase {
	return &itemTableTownFacilityUsecase{rp, logger}
}

func (uc *itemTableTownFacilityUsecase) GetItemTableTownFacility(itemNo uint32) ([]*model_item_table.ItemTableTownFacility, error) {
	itemTableTownFacilities, err := uc.rp.FindItemTableTownFacility(itemNo)
	if err != nil {
		return nil, err
	}
	return itemTableTownFacilities, nil
}
