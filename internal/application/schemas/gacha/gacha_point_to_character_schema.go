package schema_gacha

import model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"

type GachaPointToCharacterResult struct {
	BeforeDrawPoint uint32
	AfterDrawPoint  uint32
	GachaResults    []GachaResult
	ArousalResults  []interface{}
	User            model_user.User
}
