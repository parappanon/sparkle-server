## schemas

This folder contains model for transfer between layers.
For example:

- interface -> usecase
- application/usecase -> application/service
