package schema_quest

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
)

type AllQuestInfoWithClearRanksSchema struct {
	AllQuestInfo                  *model_quest.AllQuestInfo
	ClearRanks                    []*model_user.QuestClearRank
	LastPlayedChapterQuestIdPart1 int32
	LastPlayedChapterQuestIdPart2 int32
	PlayedOpenChapterIdPart1      int8
	PlayedOpenChapterIdPart2      int8
}
