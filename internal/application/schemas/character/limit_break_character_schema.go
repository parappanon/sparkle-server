package schema_character

import model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"

type LimitBreakCharacterResponseSchema struct {
	ManagedCharacter model_user.ManagedCharacter
	ItemSummary      []model_user.ItemSummary
	Gold             uint64
}
