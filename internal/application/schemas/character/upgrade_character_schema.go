package schema_character

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/upgrade"
)

type UpgradeCharacterResponseSchema struct {
	ManagedCharacter model_user.ManagedCharacter
	ItemSummary      []model_user.ItemSummary
	UpgradeResult    upgrade.CharacterUpgradeResult
	Gold             uint64
}
