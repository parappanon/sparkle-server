package encrypt

import (
	"bytes"
	"errors"
)

func pad(data []byte, blockSize int) []byte {
	padding := blockSize - len(data)%blockSize
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(data, padText...)
}

func unpad(data []byte, blockSize int) ([]byte, error) {
	length := len(data)
	if length == 0 {
		return nil, errors.New("unpad error: data is empty")
	}
	padding := int(data[length-1])
	if padding > length || padding > blockSize {
		return nil, errors.New("unpad error: padding is larger than data")
	}
	return data[:length-padding], nil
}
