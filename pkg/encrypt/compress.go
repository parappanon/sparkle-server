package encrypt

import (
	"bytes"
	"compress/zlib"
)

func compress(body []byte) []byte {
	var buf bytes.Buffer
	w := zlib.NewWriter(&buf)
	if _, err := w.Write(body); err != nil {
		panic(err)
	}
	if err := w.Close(); err != nil {
		panic(err)
	}
	return buf.Bytes()
}

func decompress(b []byte) ([]byte, error) {
	var buf bytes.Buffer
	r, err := zlib.NewReader(bytes.NewReader(b))
	if err != nil {
		return nil, err
	}
	if _, err := buf.ReadFrom(r); err != nil {
		return nil, err
	}
	if err := r.Close(); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
