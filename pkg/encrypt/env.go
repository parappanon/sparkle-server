package encrypt

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

type Key string
type Pad string

func LoadKey() Key {
	if envFilePath := os.Getenv("GO_ENV"); envFilePath != "" {
		if err := godotenv.Load(envFilePath); err != nil {
			fmt.Print("envFile " + envFilePath + "could not be loaded")
		}
	} else {
		_ = godotenv.Load(".env")
	}
	key := Key(os.Getenv("ENCRYPT_KEY"))
	return key
}

func LoadPad() Pad {
	if envFilePath := os.Getenv("GO_ENV"); envFilePath != "" {
		if err := godotenv.Load(envFilePath); err != nil {
			fmt.Print("envFile " + envFilePath + "could not be loaded")
		}
	} else {
		_ = godotenv.Load(".env")
	}
	pad := Pad(os.Getenv("ENCRYPT_PAD"))
	return pad
}
