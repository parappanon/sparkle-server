package calc

import (
	"testing"
)

func TestCumulativeRandomChoice(t *testing.T) {
	type args struct {
		weights []int64
	}
	tests := []struct {
		name string
		args args
		want map[int]int
	}{
		{
			name: "Test case 1",
			args: args{
				weights: []int64{10, 30, 60},
			},
			want: map[int]int{
				0: 95000,
				1: 295000,
				2: 595000,
			},
		},
		{
			name: "Test case 2",
			args: args{
				weights: []int64{0, 0, 100},
			},
			want: map[int]int{
				0: 0,
				1: 0,
				2: 1000000,
			},
		},
		{
			name: "Test case 3",
			args: args{
				weights: []int64{30, 30, 40},
			},
			want: map[int]int{
				0: 295000,
				1: 295000,
				2: 390000,
			},
		},
	}
	var randomSeed int64 = 1
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := 1000000
			result := make(map[int]int)
			for i := 0; i < n; i++ {
				choice := int(CumulativeRandomChoice(tt.args.weights, &randomSeed))
				result[choice]++
			}
			for i := range result {
				if tt.want[i] > result[i] {
					t.Errorf("CumulativeRandomChoice() = %v, want %v", result, tt.want)
					break
				}
			}
		})
	}
}
