package response

const SERVER_VERSION = 2211181

type ResultCode int

const (
	RESULT_ERROR                             ResultCode = -1
	RESULT_SUCCESS                           ResultCode = 0
	RESULT_OUTDATED                          ResultCode = 1
	RESULT_APPLYING                          ResultCode = 2
	RESULT_ACCESS_LIMITATION                 ResultCode = 3
	RESULT_TERMS_UPDATED                     ResultCode = 4
	RESULT_MAINTENANCE                       ResultCode = 10
	RESULT_UNAVAILABLE                       ResultCode = 20
	RESULT_OUTDATED_AB_VERSION               ResultCode = 30
	RESULT_INVALID_REQUEST_HASH              ResultCode = 101
	RESULT_INVALID_PARAMETERS                ResultCode = 102
	RESULT_INSUFFICIENT_PARAMETERS           ResultCode = 103
	RESULT_INVALID_JSON_SCHEMA               ResultCode = 104
	RESULT_DB_ERROR                          ResultCode = 105
	RESULT_NG_WORD                           ResultCode = 106
	RESULT_ALREADY_PROCESSED                 ResultCode = 107
	RESULT_PLAYER_ALREADY_EXISTS             ResultCode = 201
	RESULT_PLAYER_NOT_FOUND                  ResultCode = 202
	RESULT_PLAYER_SESSION_EXPIRED            ResultCode = 203
	RESULT_PLAYER_SUSPENDED                  ResultCode = 204
	RESULT_PLAYER_STOPPED                    ResultCode = 205
	RESULT_PLAYER_DELETED                    ResultCode = 206
	RESULT_INVALID_MOVE_PARAMETERS           ResultCode = 210
	RESULT_INVALID_LINK_PARAMETERS           ResultCode = 211
	RESULT_UUID_NOT_FOUND                    ResultCode = 212
	RESULT_INVALID_AUTH_VOIDED               ResultCode = 213
	RESULT_WEAPON_UPGRADE_LIMIT              ResultCode = 293
	RESULT_CHARACTER_UPGRADE_LIMIT           ResultCode = 294
	RESULT_FACILITY_EXT_LIMIT                ResultCode = 295
	RESULT_SUPPORT_CHAR_LIMIT                ResultCode = 296
	RESULT_SUPPORT_LIMIT                     ResultCode = 297
	RESULT_ROOM_OBJECT_EXT_LIMIT             ResultCode = 298
	RESULT_WEAPON_EXT_LIMIT                  ResultCode = 299
	RESULT_WEAPON_LIMIT                      ResultCode = 301
	RESULT_FACILITY_LIMIT                    ResultCode = 302
	RESULT_ROOM_OBJECT_LIMIT                 ResultCode = 303
	RESULT_CHARACTER_LIMIT                   ResultCode = 304
	RESULT_ITEM_LIMIT                        ResultCode = 305
	RESULT_TOWN_LIMIT                        ResultCode = 306
	RESULT_ROOM_LIMIT                        ResultCode = 307
	RESULT_LIMIT_BREAK_LIMIT                 ResultCode = 308
	RESULT_GACHA_DRAW_LIMIT                  ResultCode = 309
	RESULT_FRIEND_LIMIT                      ResultCode = 310
	RESULT_FAVORITE_ITEM                     ResultCode = 311
	RESULT_FAVORITE_WEAPON                   ResultCode = 312
	RESULT_FAVORITE_CHARACTER                ResultCode = 313
	RESULT_GACHA_STEP_DRAW_LIMIT             ResultCode = 314
	RESULT_GACHA_NOT_FREE_DRAW               ResultCode = 315
	RESULT_GACHA_POSSIBLE_FREE_DRAW          ResultCode = 316
	RESULT_WEAPON_SKILL_UP_LIMIT             ResultCode = 317
	RESULT_ALREADY_OWN_CHARACTER             ResultCode = 331
	RESULT_ALREADY_LOAD_WEAPON               ResultCode = 332
	RESULT_ALREADY_OWN_ORB                   ResultCode = 333
	RESULT_FRIEND_PROPOSE_LIMIT              ResultCode = 334
	RESULT_CHEST_OUT_OF_PERIOD               ResultCode = 351
	RESULT_CHEST_CAN_NOT_RESET               ResultCode = 352
	RESULT_CHEST_EMPTY_REWARD                ResultCode = 353
	RESULT_FRIEND_LIMIT_TARGET               ResultCode = 354
	RESULT_GOLD_IS_SHORT                     ResultCode = 441
	RESULT_ITEM_IS_SHORT                     ResultCode = 442
	RESULT_UNLIMITED_GEM_IS_SHORT            ResultCode = 443
	RESULT_LIMITED_GEM_IS_SHORT              ResultCode = 444
	RESULT_GEM_IS_SHORT                      ResultCode = 445
	RESULT_KIRARA_IS_SHORT                   ResultCode = 446
	RESULT_KIRARA_LIMIT                      ResultCode = 447
	RESULT_ITEM_IS_EXPIRED                   ResultCode = 448
	RESULT_STAMINA_IS_SHORT                  ResultCode = 451
	RESULT_COST_IS_SHORT                     ResultCode = 452
	RESULT_QUEST_OUT_OF_PERIOD               ResultCode = 461
	RESULT_GACHA_OUT_OF_PERIOD               ResultCode = 462
	RESULT_TRADE_OUT_OF_PERIOD               ResultCode = 463
	RESULT_BUY_OUT_OF_PERIOD                 ResultCode = 464
	RESULT_QUEST_RETRY_LIMIT                 ResultCode = 465
	RESULT_QUEST_ORDER_LIMIT                 ResultCode = 466
	RESULT_BARGAIN_OUT_OF_PERIOD             ResultCode = 467
	RESULT_PRESENT_OUT_OF_PERIOD             ResultCode = 468
	RESULT_MORE_STAMINA_NEEDED_THAN_EXPECTED ResultCode = 469
	RESULT_NOT_YET_FRIEND                    ResultCode = 501
	RESULT_ALREADY_FRIEND                    ResultCode = 502
	RESULT_FRIEND_NOT_REQUEST                ResultCode = 503
	RESULT_ALREADY_COMPLETE_MISSION          ResultCode = 510
	RESULT_ALREADY_FRIEND_REQUEST            ResultCode = 511
	RESULT_ALREADY_FRIEND_REQUEST_RECEIVED   ResultCode = 512
	RESULT_CURL_CONNECTION_TIMEOUT           ResultCode = 601
	RESULT_CURL_CONNECTION_ERROR             ResultCode = 602
	RESULT_APP_STORE_ERROR                   ResultCode = 603
	RESULT_GOOGLE_PLAY_ERROR                 ResultCode = 604
	RESULT_APP_STORE_INVALID_RECEIPT         ResultCode = 605
	RESULT_GOOGLE_PLAY_INVALID_STATUS        ResultCode = 606
	RESULT_STORE_ITEM_NOT_FOUND              ResultCode = 607
	RESULT_STORE_ALREADY_PURCHASED           ResultCode = 608
	RESULT_PURCHASE_GEM_OVERFLOW             ResultCode = 609
	RESULT_EXCHANGE_SHOP_OUT_OF_PERIOD       ResultCode = 651
	RESULT_EXCHANGE_SHOP_LIMIT               ResultCode = 652
	RESULT_TRAINING_COMPLETE_TIME_ERROR      ResultCode = 701
	RESULT_TRAINING_ALREADY_COMPLETED        ResultCode = 702
	RESULT_TRAINING_OUT_OF_PERIOD            ResultCode = 703
	RESULT_PROMOTION_CODE_NOT_FOUND          ResultCode = 801
	RESULT_PROMOTION_CODE_ALREADY_USED       ResultCode = 802
	RESULT_PROMOTION_CODE_OF_SAME_CAMPAIGN   ResultCode = 803
	RESULT_PROMOTION_CAMPAIGN_NOT_OPEN       ResultCode = 804
	RESULT_PROMOTION_CAMPAIGN_ALREADY_OPEN   ResultCode = 805
	RESULT_UNKNOWN_ERROR                     ResultCode = 999
)
