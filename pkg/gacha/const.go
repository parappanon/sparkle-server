package gacha

type Rarity int

const (
	Rarity5 Rarity = 5
	Rarity4 Rarity = 4
	Rarity3 Rarity = 3
)

type GachaProbability int

const (
	Rarity5Prob GachaProbability = 2  // レアリティ5の排出確率(%)
	Rarity4Prob GachaProbability = 12 // レアリティ4の排出確率(%)
	Rarity3Prob GachaProbability = 86 // レアリティ3の排出確率(%)

	Rarity5ProbUpOnce GachaProbability = 4  // timesが1かつisChanceUpがtrueの場合のレアリティ5の排出確率(%)
	Rarity4ProbUpOnce GachaProbability = 24 // timesが1かつisChanceUpがtrueの場合のレアリティ4の排出確率(%)
	Rarity3ProbUpOnce GachaProbability = 72 // timesが1かつisChanceUpがtrueの場合のレアリティ3の排出確率(%)

	Rarity5ProbUpTen GachaProbability = 5  // isChanceUpがtrueかつ10連の場合のレアリティ5の排出確率(%)
	Rarity4ProbUpTen GachaProbability = 30 // isChanceUpがtrueかつ10連の場合のレアリティ4の排出確率(%)
	Rarity3ProbUpTen GachaProbability = 65 // isChanceUpがtrueかつ10連の場合のレアリティ3の排出確率(%)

	Rarity5FixedProb GachaProbability = 2  // 確定枠のレアリティ5の排出確率(%)
	Rarity4FixedProb GachaProbability = 98 // 確定枠のレアリティ4の排出確率(%)

	Rarity5FixedProbUp GachaProbability = 5  // isChanceUpがtrueの場合の確定枠のレアリティ5の排出確率(%)
	Rarity4FixedProbUp GachaProbability = 95 // isChanceUpがtrueの場合の確定枠のレアリティ4の排出確率(%)
)

type GachaCharacterDrops map[Rarity][]uint32
type GachaCharacterRarity []Rarity
