package gacha

import (
	"errors"
	"math/rand"
	"time"
)

type GachaRarityHandler struct {
	randomGenerator *rand.Rand
}

func NewGachaRarityHandler(randSource *rand.Source) GachaRarityHandler {
	if randSource != nil {
		return GachaRarityHandler{
			randomGenerator: rand.New(*randSource),
		}
	}
	return GachaRarityHandler{
		randomGenerator: rand.New(rand.NewSource(time.Now().UnixNano())),
	}
}

// ContainsRarityは、指定されたスライスに指定された値が含まれているかどうかを返す関数です。
func (h *GachaRarityHandler) ContainsRarity(s []Rarity, e Rarity) bool {
	for _, v := range s {
		if v == e {
			return true
		}
	}
	return false
}

// rollSingleは1回分のガチャを回し、結果を返す関数です。
// isChanceUp: trueの場合、排出確率が上昇します。
func (h *GachaRarityHandler) rollSingle(isChanceUp bool) Rarity {
	r := GachaProbability(h.randomGenerator.Intn(100))
	if isChanceUp {
		if r < Rarity5ProbUpOnce {
			return Rarity5
		}
		if r < Rarity5ProbUpOnce+Rarity4ProbUpOnce {
			return Rarity4
		}
		return Rarity3
	}
	if r < Rarity5Prob {
		return Rarity5
	}
	if r < Rarity5Prob+Rarity4Prob {
		return Rarity4
	}
	return Rarity3
}

// rollSingleは10回分のガチャを回し、結果を返す関数です。
// isChanceUp: trueの場合、排出確率が上昇します。
func (h *GachaRarityHandler) rollTen(isChanceUp bool) []Rarity {
	res := make([]Rarity, 10)
	for i := 0; i < 10; i++ {
		r := GachaProbability(h.randomGenerator.Intn(100))
		if isChanceUp {
			if r < Rarity5ProbUpTen {
				res[i] = Rarity5
			} else if r < Rarity5ProbUpTen+Rarity4ProbUpTen {
				res[i] = Rarity4
			} else {
				res[i] = Rarity3
			}
		} else {
			res[i] = h.rollSingle(isChanceUp)
		}
	}
	// ガチャを10回まとめて回して、レアリティ4または5が1度も出なかった場合の処理
	if !h.ContainsRarity(res, Rarity4) && !h.ContainsRarity(res, Rarity5) {
		res[9] = h.rollRareOnly(isChanceUp)
	}
	return res
}

// rollRareは10回まとめてガチャを回した場合、レアリティ4または5が1度も出なかった場合に、10回目の結果を返す関数です。
// isChanceUp: trueの場合、排出確率が上昇します。
func (h *GachaRarityHandler) rollRareOnly(isChanceUp bool) Rarity {
	r := GachaProbability(h.randomGenerator.Intn(100))
	if isChanceUp {
		if r <= Rarity5FixedProbUp {
			return Rarity5
		}
		return Rarity4
	}
	if r <= Rarity5FixedProb {
		return Rarity5
	}
	return Rarity4
}

// Rollは指定された回数分のガチャを回し、結果を返す関数です。
// times: ガチャを回す回数
// isChanceUp: trueの場合、排出確率が上昇します。
func (h *GachaRarityHandler) Roll(times int, isChanceUp bool) ([]Rarity, error) {
	if times < 1 || times > 10 {
		return nil, errors.New("times must be 1 to 10")
	}
	if times == 10 {
		return h.rollTen(isChanceUp), nil
	}
	results := make([]Rarity, times)
	for i := 0; i < times; i++ {
		results[i] = h.rollSingle(isChanceUp)
	}
	return results, nil
}

// RollRareOnlyは指定された回数分のガチャを回し、結果を返す関数です。
// Rollと違い、Rarity4またはRarity5のみが出現します。
// times: ガチャを回す回数
// isChanceUp: trueの場合、排出確率が上昇します。
func (h *GachaRarityHandler) RollRareOnly(times int, isChanceUp bool) ([]Rarity, error) {
	if times < 1 || times > 10 {
		return nil, errors.New("times must be 1 to 10")
	}
	results := make([]Rarity, times)
	for i := 0; i < times; i++ {
		results[i] = h.rollRareOnly(isChanceUp)
	}
	return results, nil
}
