package gacha_test

import (
	"math"
	"math/rand"
	"reflect"
	"testing"

	"gitlab.com/kirafan/sparkle/server/pkg/gacha"
)

func TestGachaRarityHandlerProbability(t *testing.T) {
	// ランダムシードを指定
	source := rand.NewSource(123)
	handler := gacha.NewGachaRarityHandler(&source)

	// ガチャを1000回実行し、レアリティ5、4、3の排出確率が意図したものに近似しているかをテスト
	const times = 1000
	const delta = 0.1 // 許容誤差
	var count5, count4, count3 int
	for i := 0; i < times; i++ {
		result, err := handler.Roll(1, false)
		if err != nil {
			t.Errorf("gacha(1, false) = %v, want no error", err)
		}
		switch result[0] {
		case gacha.Rarity5:
			count5++
		case gacha.Rarity4:
			count4++
		case gacha.Rarity3:
			count3++
		default:
			t.Errorf("invalid rarity: %d", result)
		}
	}
	prob5 := float64(count5) / float64(times) * 100
	prob4 := float64(count4) / float64(times) * 100
	prob3 := float64(count3) / float64(times) * 100
	if math.Abs(prob5-float64(gacha.Rarity5Prob)) > float64(gacha.Rarity5Prob)+delta {
		t.Errorf("rarity5 probability = %f, want %f", prob5, float64(gacha.Rarity5Prob))
	}
	if math.Abs(prob4-float64(gacha.Rarity4Prob)) > float64(gacha.Rarity4Prob)+delta {
		t.Errorf("rarity4 probability = %f, want %f", prob4, float64(gacha.Rarity4Prob))
	}
	if math.Abs(prob3-float64(gacha.Rarity3Prob)) > float64(gacha.Rarity3Prob)+delta {
		t.Errorf("rarity3 probability = %f, want %f", prob3, float64(gacha.Rarity3Prob))
	}
}

func TestGachaRarityHandlerBase(t *testing.T) {
	// ランダムシードを指定
	source := rand.NewSource(123)
	handler := gacha.NewGachaRarityHandler(&source)

	// isChanceUpがfalseで、ガチャを1回回す場合
	result, err := handler.Roll(1, false)
	if err != nil {
		t.Errorf("gacha(1, false) = %v, want no error", err)
	}
	if len(result) != 1 {
		t.Errorf("gacha(1, false) = %v, want %v", result, []gacha.Rarity{3})
	}
	t.Logf("gacha(times=1, isChanceUp=false) = %v", result)

	// isChanceUpがtrueで、ガチャを1回回す場合
	result, err = handler.Roll(1, true)
	if err != nil {
		t.Errorf("gacha(1, true) = %v, want no error", err)
	}
	if !reflect.DeepEqual(result, []gacha.Rarity{3}) {
		t.Errorf("gacha(1, true) = %v, want %v", result, []gacha.Rarity{3})
	}
	t.Logf("gacha(times=1, isChanceUp=true) = %v", result)

	// isChanceUpがfalseで、ガチャを10回まとめて回す場合
	result, err = handler.Roll(10, false)
	if err != nil {
		t.Errorf("gacha(times=10, isChanceUp=false) = %v, want no error", err)
	}
	if !reflect.DeepEqual(result, []gacha.Rarity{3, 3, 3, 4, 4, 3, 5, 3, 3, 3}) {
		t.Errorf("gacha(times=10, isChanceUp=false) = %v, want %v", result, []gacha.Rarity{3, 3, 3, 4, 4, 3, 5, 3, 3, 3})
	}
	t.Logf("gacha(times=10, isChanceUp=false) = %v", result)

	// isChanceUpがtrueで、ガチャを10回まとめて回す場合
	result, err = handler.Roll(10, true)
	if err != nil {
		t.Errorf("gacha(times=10, isChanceUp=true) = %v, want no error", err)
	}
	if !reflect.DeepEqual(result, []gacha.Rarity{3, 3, 4, 4, 3, 4, 4, 3, 4, 3}) {
		t.Errorf("gacha(times=10, isChanceUp=true) = %v, want %v", result, []gacha.Rarity{3, 3, 4, 4, 3, 4, 4, 3, 4, 3})
	}
	t.Logf("gacha(times=10, isChanceUp=true) = %v", result)

	// ガチャを10回まとめて回したとき、レアリティ4または5が必ず1度出現することをテスト
	for i := 0; i < 50; i++ {
		result, err = handler.Roll(10, false)
		if err != nil {
			t.Errorf("gacha(10, false) = %v, want no error", err)
		}
		if !handler.ContainsRarity(result, gacha.Rarity4) && !handler.ContainsRarity(result, gacha.Rarity5) {
			t.Errorf("gacha(10, false) with no rarity4 or 5 = %v, want at least one rarity4 or 5", result)
		}
	}
}
