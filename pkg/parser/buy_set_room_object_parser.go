package parser

import (
	"strconv"
	"strings"
)

func ParseToManagedRoomObjectIdsResponse(newManagedRoomObjectIds []int) string {
	// Format newManagedRoomObjectIds as string array
	managedRoomObjectIdsStr := make([]string, len(newManagedRoomObjectIds))
	for i := range newManagedRoomObjectIds {
		managedRoomObjectIdsStr[i] = strconv.Itoa(newManagedRoomObjectIds[i])
	}
	return strings.Join(managedRoomObjectIdsStr, ",")
}
