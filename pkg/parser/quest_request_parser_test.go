package parser

import (
	"reflect"
	"testing"
)

func TestParseCharacterExpString(t *testing.T) {
	type args struct {
		requestString string
	}
	tests := []struct {
		name string
		args args
		want map[uint64]SkillResult
	}{
		{
			name: "TestParseCharacterExpString with valid string success",
			args: args{
				requestString: "5495779146:2:0:0,5495808105:1:1:1,5495767092:0:0:0,5495808117:1:1:1,5495808118:0:0:0",
			},
			want: map[uint64]SkillResult{
				5495779146: {
					Skill1: 2,
					Skill2: 0,
					Skill3: 0,
				},
				5495808105: {
					Skill1: 1,
					Skill2: 1,
					Skill3: 1,
				},
				5495767092: {
					Skill1: 0,
					Skill2: 0,
					Skill3: 0,
				},
				5495808117: {
					Skill1: 1,
					Skill2: 1,
					Skill3: 1,
				},
				5495808118: {
					Skill1: 0,
					Skill2: 0,
					Skill3: 0,
				},
			},
		},
		{
			name: "TestParseCharacterExpString with empty string success",
			args: args{
				requestString: "",
			},
			want: map[uint64]SkillResult{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseCharacterExpString(tt.args.requestString); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseCharacterExpString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseWeaponExpString(t *testing.T) {
	type args struct {
		requestString string
	}
	tests := []struct {
		name string
		args args
		want map[uint64]SkillResult
	}{
		{
			name: "TestParseWeaponExpString for character skills",
			args: args{
				requestString: "7291034:1,7294080:1,7291035:0,7291343:1,7294081:0",
			},
			want: map[uint64]SkillResult{
				7291034: {
					Skill1: 1,
					Skill2: 0,
					Skill3: 0,
				},
				7294080: {
					Skill1: 1,
					Skill2: 0,
					Skill3: 0,
				},
				7291035: {
					Skill1: 0,
					Skill2: 0,
					Skill3: 0,
				},
				7291343: {
					Skill1: 1,
					Skill2: 0,
					Skill3: 0,
				},
				7294081: {
					Skill1: 0,
					Skill2: 0,
					Skill3: 0,
				},
			},
		},
		{
			name: "TestParseWeaponExpString with empty string success",
			args: args{
				requestString: "",
			},
			want: map[uint64]SkillResult{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseWeaponExpString(tt.args.requestString); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseWeaponExpString() = %v, want %v", got, tt.want)
			}
		})
	}
}
